package com.realstudios.gkb;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.realstudios.gkb.adapter.AvatarFragmentAdapter;

import org.jetbrains.annotations.NotNull;

public class ProfilePhotoActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager2 viewPager;
    boolean isHomePageEnabled = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_photo);

        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);

        isHomePageEnabled =  GKBApp.FirebaseRemoteConfig().getBoolean("homepage");

        AvatarFragmentAdapter avatarFragmentAdapter = new AvatarFragmentAdapter(getSupportFragmentManager(),getLifecycle());
        viewPager.setAdapter(avatarFragmentAdapter);

        new TabLayoutMediator(tabLayout, viewPager, (tab, position) -> {}).attach();

        findViewById(R.id.tv_submit).setOnClickListener(view -> {

            if(isHomePageEnabled) {
                startActivity(new Intent(ProfilePhotoActivity.this, MainActivity.class));
                finish();
            }
            else {
                Toast.makeText(this, "FEATURE NOT ENABLED. TRY AGAIN LATER", Toast.LENGTH_LONG).show();
            }
        });
    }
}