package com.realstudios.gkb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.realstudios.gkb.adapter.ReadingAdapter;
import com.realstudios.gkb.model.Book;
import com.realstudios.gkb.model.BookType;
import com.realstudios.gkb.model.Reading;

import java.util.ArrayList;
import java.util.List;

public class ContinueActivity extends AppCompatActivity implements ReadingAdapter.setOnReadingListener{

    RecyclerView recyclerView;
    ReadingAdapter adapter;
    List<Reading> readings;
    List<Book> books;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_continue);


        findViewById(R.id.view_back).setOnClickListener(view -> finish());
        findViewById(R.id.btn_back).setOnClickListener(view -> finish());
        recyclerView = findViewById(R.id.recyclerView);
        books = new ArrayList<>();
        readings = new ArrayList<>();

        books.add(new Book(1, "Talk like TED", "Carmine Gallo", R.drawable.talk_like_ted, BookType.BOOK));
        books.add(new Book(2, "Never Split the Difference", "Chris Voss", R.drawable.never_split_the_diff, BookType.AUDIOBOOK));
        books.add(new Book(3, "King Peggy", "Peggylin Bartels", R.drawable.king_peggy_big, BookType.VIDEO));
        books.add(new Book(4, "Think like Einstein", "Peter Hollins", R.drawable.think_like_einstein, BookType.BOOK));
        books.add(new Book(5, "Will it Fly?", "Pat Flynn", R.drawable.will_it_fly, BookType.BOOK));
        books.add(new Book(6, "Design a Better business", "Eric Vanderbuilt", R.drawable.design_better_business, BookType.AUDIOBOOK));
        books.add(new Book(7, "Think like Einstein", "Peter Hollins", R.drawable.persuasion_iq, BookType.BOOK));
        books.add(new Book(8, "The AI Advantage", "Thomas H. Davenport", R.drawable.ai_advantage, BookType.VIDEO));
        books.add(new Book(9, "Power VS Force", "David R. Hawkins PhD.", R.drawable.power_vs_force, BookType.VIDEO));
        books.add(new Book(10, "Persuasion IQ", "Kurt W. Mortensen", R.drawable.persuasion_iq, BookType.BOOK));
        books.add(new Book(11, "The AI Advantage", "Thomas H. Davenport", R.drawable.ai_advantage, BookType.AUDIOBOOK));
        books.add(new Book(12, "Power VS Force", "David R. Hawkins PhD.", R.drawable.power_vs_force, BookType.ARTICLE));
        books.add(new Book(13, "Talk like TED", "Carmine Gallo", R.drawable.talk_like_ted, BookType.BOOK));
        books.add(new Book(14, "Never Split the Difference", "Chris Voss", R.drawable.never_split_the_diff, BookType.AUDIOBOOK));
        books.add(new Book(15, "King Peggy", "Peggylin Bartels", R.drawable.king_peggy_big, BookType.VIDEO));
        books.add(new Book(16, "Think like Einstein", "Peter Hollins", R.drawable.think_like_einstein, BookType.BOOK));


        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ReadingAdapter(this,this,getReadings());
        recyclerView.setAdapter(adapter);

    }

    public List<Reading> getReadings() {

        for (int i =0; i<books.size(); i++){
            readings.add(new Reading(i, books.get(i),28));
        }
        return readings;
    }

    @Override
    public void onReadingSelected(Reading reading) {

    }
}