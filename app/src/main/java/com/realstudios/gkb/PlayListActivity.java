package com.realstudios.gkb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;
import androidx.viewpager2.widget.ViewPager2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.FrameLayout;

import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.realstudios.gkb.adapter.MainFragmentsAdapter;
import com.realstudios.gkb.adapter.PlayerFragmentsAdapter;
import com.realstudios.gkb.databinding.ActivityPlayListBinding;
import com.realstudios.gkb.fragments.VideoDetailFragment;
import com.realstudios.gkb.model.VideoObject;
import com.realstudios.gkb.player.Player;
import com.realstudios.gkb.player.event.OnKeyDownListener;
import com.realstudios.gkb.player.helper.PlayerHolder;
import com.realstudios.gkb.player.playqueue.PlayQueue;
import com.realstudios.gkb.player.playqueue.PlayQueueItem;
import com.realstudios.gkb.player.playqueue.PlaylistPlayQueue;
import com.realstudios.gkb.util.BackPressable;
import com.realstudios.gkb.util.Localization;
import com.realstudios.gkb.util.NavigationHelper;
import com.realstudios.gkb.util.SerializedCache;
import com.realstudios.gkb.util.StateSaver;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;

import org.schabi.newpipe.extractor.NewPipe;
import org.schabi.newpipe.extractor.Page;
import org.schabi.newpipe.extractor.StreamingService;
import org.schabi.newpipe.extractor.playlist.PlaylistInfoItem;
import org.schabi.newpipe.extractor.stream.StreamInfo;
import org.schabi.newpipe.extractor.stream.StreamInfoItem;
import org.schabi.newpipe.extractor.stream.StreamType;
import org.schabi.newpipe.extractor.stream.VideoStream;
import org.schabi.newpipe.util.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.realstudios.gkb.util.Localization.assureCorrectAppLanguage;

public class PlayListActivity extends AppCompatActivity {

    private static final String TAG = PlayListActivity.class.getSimpleName();
    private boolean servicesShown = false;
    @SuppressWarnings("ConstantConditions")
    public static final boolean DEBUG = !BuildConfig.BUILD_TYPE.equals("release");

    ActivityPlayListBinding playListBinding;
    private BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        playListBinding = ActivityPlayListBinding.inflate(getLayoutInflater());
       // setContentView(R.layout.activity_play_list);

        int serviceId = 0;



       /* StreamInfo streamInfo = new StreamInfo(serviceId,url, url, StreamType.VIDEO_STREAM, UUID.randomUUID().toString(),
                "INTRODUCTION TO THE SCIENCE LABORATORY",13);
        PlayQueue playQueue = new SinglePlayQueue(streamInfo);*/

        String imageUrl = "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/";

        List<VideoObject> videoObjects = new ArrayList<>();
        videoObjects.add(new VideoObject("Sample Decrypted media 1","file:///data/user/0/com.realstudios.gkb/cache/GKB-4892765460620564851.mp4", ""));
        videoObjects.add(new VideoObject("Big Buck Bunny",imageUrl+"BigBuckBunny.mp4",imageUrl+"/images/BigBuckBunny.jpg"));
        videoObjects.add(new VideoObject("Elephant Dream",imageUrl+"ElephantsDream.mp4",imageUrl+"/images/ElephantsDream.jpg"));
        videoObjects.add(new VideoObject("For Bigger Blazes",imageUrl+"ForBiggerBlazes.mp4",imageUrl+"/images/ForBiggerBlazes.jpg"));
        videoObjects.add(new VideoObject("For Bigger Escape",imageUrl+"ForBiggerEscapes.mp4",imageUrl+"/images/ForBiggerEscapes.jpg"));
        videoObjects.add(new VideoObject("For Bigger Fun",imageUrl+"ForBiggerFun.mp4",imageUrl+"/images/ForBiggerFun.jpg"));
        videoObjects.add(new VideoObject("For Bigger Joyrides",imageUrl+"ForBiggerJoyrides.mp4",imageUrl+"/images/ForBiggerJoyrides.jpg"));
        videoObjects.add(new VideoObject("For Bigger Meltdowns",imageUrl+"ForBiggerMeltdowns.mp4",imageUrl+"/images/ForBiggerMeltdowns.jpg"));
        videoObjects.add(new VideoObject("Sintel",imageUrl+"Sintel.mp4",imageUrl+"/images/Sintel.jpg"));
        videoObjects.add(new VideoObject("Subaru Outback On Street And Dirt",imageUrl+"SubaruOutbackOnStreetAndDirt.mp4",imageUrl+"/images/BigBuckBunny.jpg"));
        videoObjects.add(new VideoObject("Tears of Steel",imageUrl+"TearsOfSteel.mp4",imageUrl+"/images/BigBuckBunny.jpg"));
        videoObjects.add(new VideoObject("Volkswagen GTI Review",imageUrl+"VolkswagenGTIReview.mp4",imageUrl+"/images/BigBuckBunny.jpg"));
        videoObjects.add(new VideoObject("We Are Going On Bullrun",imageUrl+"WeAreGoingOnBullrun.mp4",imageUrl+"/images/BigBuckBunny.jpg"));
        videoObjects.add(new VideoObject("What care can you get for a grand?",imageUrl+"WhatCarCanYouGetForAGrand.mp4",imageUrl+"/images/BigBuckBunny.jpg"));

        PlaylistInfoItem playlistInfoItem = new PlaylistInfoItem(0,"","Big Buck Bunny");
        PlayQueue playQueue1 = new PlaylistPlayQueue(playlistInfoItem);
        for (VideoObject videoObject: videoObjects) {
            //StreamInfoItem infoItem = new StreamInfoItem(serviceId, videoObject.getSource(), videoObject.getTitle(), StreamType.VIDEO_STREAM);
            //infoItem.setUploaderName(videoObject.getTitle());

            //streamInfoItems.add(infoItem);
            PlayQueueItem playQueueItem = new PlayQueueItem(videoObject.getTitle(),
                    videoObject.getSource(),serviceId,0,"",videoObject.getTitle(),"",StreamType.VIDEO_STREAM);

            playQueue1.append(playQueueItem);
        }



        NavigationHelper.openVideoDetailFragment(
                getApplicationContext(), getSupportFragmentManager(),
                serviceId, playQueue1.getItem(playQueue1.size()-1).getUrl(), playQueue1.getItem(playQueue1.size()-1).getTitle(), playQueue1, true);


        openMiniPlayerUponPlayerStarted();

    }

    private void openMiniPlayerIfMissing() {
        final Fragment fragmentPlayer = getSupportFragmentManager()
                .findFragmentById(R.id.fragment_player_holder);
        if (fragmentPlayer == null) {
            // We still don't have a fragment attached to the activity. It can happen when a user
            // started popup or background players without opening a stream inside the fragment.
            // Adding it in a collapsed state (only mini player will be visible).
            NavigationHelper.showMiniPlayer(getSupportFragmentManager());
        }
    }

    private void openMiniPlayerUponPlayerStarted() {
        if (getIntent().getSerializableExtra(Constants.KEY_LINK_TYPE)
                == StreamingService.LinkType.STREAM) {
            // handleIntent() already takes care of opening video detail fragment
            // due to an intent containing a STREAM link
            return;
        }

        if (PlayerHolder.getInstance().isPlayerOpen()) {
            // if the player is already open, no need for a broadcast receiver
            openMiniPlayerIfMissing();
        } else {
            // listen for player start intent being sent around
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(final Context context, final Intent intent) {
                    if (Objects.equals(intent.getAction(),
                            VideoDetailFragment.ACTION_PLAYER_STARTED)) {
                        openMiniPlayerIfMissing();
                        // At this point the player is added 100%, we can unregister. Other actions
                        // are useless since the fragment will not be removed after that.
                        unregisterReceiver(broadcastReceiver);
                        broadcastReceiver = null;
                    }
                }
            };
            final IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(VideoDetailFragment.ACTION_PLAYER_STARTED);
            registerReceiver(broadcastReceiver, intentFilter);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!isChangingConfigurations()) {
            StateSaver.clearStateFiles();
        }
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }


    @Override
    protected void onResume() {
        assureCorrectAppLanguage(this);
        // Change the date format to match the selected language on resume
        Localization.initPrettyTime(Localization.resolvePrettyTime(getApplicationContext()));
        super.onResume();


        final SharedPreferences sharedPreferences
                = PreferenceManager.getDefaultSharedPreferences(this);


        if (sharedPreferences.getBoolean(Constants.KEY_MAIN_PAGE_CHANGE, false)) {
            if (DEBUG) {
                Log.d(TAG, "main page has changed, recreating main fragment...");
            }
            sharedPreferences.edit().putBoolean(Constants.KEY_MAIN_PAGE_CHANGE, false).apply();
            NavigationHelper.openMainActivity(this);
        }


    }

    @Override
    protected void onNewIntent(final Intent intent) {
        if (DEBUG) {
            Log.d(TAG, "onNewIntent() called with: intent = [" + intent + "]");
        }
        if (intent != null) {
            // Return if launched from a launcher (e.g. Nova Launcher, Pixel Launcher ...)
            // to not destroy the already created backstack
            final String action = intent.getAction();
            if ((action != null && action.equals(Intent.ACTION_MAIN))
                    && intent.hasCategory(Intent.CATEGORY_LAUNCHER)) {
                return;
            }
        }

        super.onNewIntent(intent);
        setIntent(intent);
        handleIntent(intent);
    }

    @Override
    public boolean onKeyDown(final int keyCode, final KeyEvent event) {
        final Fragment fragment = getSupportFragmentManager()
                .findFragmentById(R.id.fragment_player_holder);
        if (fragment instanceof OnKeyDownListener) {
            // Provide keyDown event to fragment which then sends this event
            // to the main player service
            return ((OnKeyDownListener) fragment).onKeyDown(keyCode)
                    || super.onKeyDown(keyCode, event);
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        if (DEBUG) {
            Log.d(TAG, "onBackPressed() called");
        }
        final Fragment fragment = getSupportFragmentManager()
                .findFragmentById(R.id.fragment_player_holder);
        // If current fragment implements BackPressable (i.e. can/wanna handle back press)
        // delegate the back press to it
        if (fragment instanceof BackPressable) {
            Log.d(TAG, "fragment is an instance of BackPressable:"+ fragment);
            if (((BackPressable) fragment).onBackPressed()) {
                return;
            }
            else {

            }
        }
        else {
            Log.d(TAG, "fragment is not an instance of BackPressable:"+ fragment);
        }
    }


    private void handleIntent(final Intent intent) {
        try {
            if (DEBUG) {
                Log.d(TAG, "handleIntent() called with: intent = [" + intent + "]");
            }

            if (intent.hasExtra(Constants.KEY_LINK_TYPE)) {
                final String url = intent.getStringExtra(Constants.KEY_URL);
                final int serviceId = intent.getIntExtra(Constants.KEY_SERVICE_ID, 0);
                String title = intent.getStringExtra(Constants.KEY_TITLE);
                if (title == null) {
                    title = "";
                }

                final StreamingService.LinkType linkType = ((StreamingService.LinkType) intent
                        .getSerializableExtra(Constants.KEY_LINK_TYPE));
                assert linkType != null;
                switch (linkType) {
                    case STREAM:
                        final String intentCacheKey = intent.getStringExtra(
                                Player.PLAY_QUEUE_KEY);
                        final PlayQueue playQueue = intentCacheKey != null
                                ? SerializedCache.getInstance()
                                .take(intentCacheKey, PlayQueue.class)
                                : null;

                        final boolean switchingPlayers = intent.getBooleanExtra(
                                VideoDetailFragment.KEY_SWITCHING_PLAYERS, false);
                        NavigationHelper.openVideoDetailFragment(
                                getApplicationContext(), getSupportFragmentManager(),
                                serviceId, url, title, playQueue, switchingPlayers);
                        break;
                    case PLAYLIST:
                        NavigationHelper.openPlaylistFragment(getSupportFragmentManager(),
                                serviceId, url, title);
                        break;
                }
            } else {
                NavigationHelper.gotoMainFragment(getSupportFragmentManager());
            }
        } catch (final Exception e) {
            //ErrorActivity.reportUiErrorInSnackbar(this, "Handling intent", e);
        }
    }

}