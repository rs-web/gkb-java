package com.realstudios.gkb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import com.realstudios.gkb.adapter.BooksFragmentAdapter;
import com.realstudios.gkb.adapter.SearchResultFragmentAdapter;
import com.realstudios.gkb.model.BookType;

import java.util.ArrayList;
import java.util.List;

public class SearchResultActivity extends AppCompatActivity {


    TabLayout tabLayout;
    ViewPager2 viewPager;
    String searchString, searchResultFor;
    AppCompatTextView tvSearchResultFor;
    String noResult ="No Book found for ...'this shows how the text wraps into 2nd line'";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);
        tvSearchResultFor = findViewById(R.id.tv_search_result_for);
        findViewById(R.id.view_back).setOnClickListener(view -> finish());
        findViewById(R.id.btn_back).setOnClickListener(view -> finish());

        searchString =getIntent().getStringExtra("searchString");

        searchResultFor = "Result for ...'"+ searchString+ "'";

        tvSearchResultFor.setText(searchResultFor);

        List<String> bookTypes = new ArrayList<>();
        bookTypes.add(BookType.BOOK);
        bookTypes.add(BookType.AUDIOBOOK);
        bookTypes.add(BookType.VIDEO);
        bookTypes.add(BookType.ARTICLE);
        SearchResultFragmentAdapter booksFragmentAdapter = new SearchResultFragmentAdapter(getSupportFragmentManager(),getLifecycle(),bookTypes, searchString);



        //viewPager.setUserInputEnabled(false);
        viewPager.setAdapter(booksFragmentAdapter);

        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                tabLayout.selectTab(tabLayout.getTabAt(position));
                super.onPageSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition(),true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition(),true);
            }
        });


    }
}