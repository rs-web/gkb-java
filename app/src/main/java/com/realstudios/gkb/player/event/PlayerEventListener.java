package com.realstudios.gkb.player.event;


import com.google.android.exoplayer2.PlaybackParameters;
import com.realstudios.gkb.player.playqueue.PlayQueue;


import org.schabi.newpipe.extractor.stream.StreamInfo;


public interface PlayerEventListener {
    void onQueueUpdate(PlayQueue queue);
    void onPlaybackUpdate(int state, int repeatMode, boolean shuffled,
                          PlaybackParameters parameters);
    void onProgressUpdate(int currentProgress, int duration, int bufferPercent);
    void onMetadataUpdate(StreamInfo info, PlayQueue queue);
    void onServiceStopped();
}
