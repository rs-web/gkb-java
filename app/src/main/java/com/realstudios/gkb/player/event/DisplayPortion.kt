package com.realstudios.gkb.player.event

enum class DisplayPortion {
    LEFT, MIDDLE, RIGHT, LEFT_HALF, RIGHT_HALF
}
