package com.realstudios.gkb.player.event;


import com.realstudios.gkb.player.MainPlayer;
import com.realstudios.gkb.player.Player;

public interface PlayerServiceExtendedEventListener extends PlayerServiceEventListener {
    void onServiceConnected(Player player,
                            MainPlayer playerService,
                            boolean playAfterConnect);
    void onServiceDisconnected();
}
