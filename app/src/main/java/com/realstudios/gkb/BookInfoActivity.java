package com.realstudios.gkb;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;

import com.google.android.material.behavior.HideBottomViewOnScrollBehavior;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.realstudios.gkb.adapter.HorizontalBooksAdapter;
import com.realstudios.gkb.model.Book;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static com.realstudios.gkb.Utils.convertDpToPixelInt;

public class BookInfoActivity extends AppCompatActivity implements HorizontalBooksAdapter.setOnBookListener{

    List<Book> books;
    HorizontalBooksAdapter booksAdapter;
    RecyclerView recyclerViewBooks;
    AppCompatTextView tvChargeNotice, tvDownloadNotice,btnBuyBook, btnPreview, btnDownload,bntDownload1;
    ViewFlipper viewFlipperMain, viewFlipperBuy, viewFlipperDownload;
    View onBoardingBottomSheet;
    //BottomSheetBehavior<View> sheetBehavior;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_info);

        viewFlipperMain = findViewById(R.id.view_flipper_buy_book);
        viewFlipperDownload = findViewById(R.id.view_flipper_download);
        viewFlipperBuy = findViewById(R.id.view_flipper_buy);
        findViewById(R.id.view_back).setOnClickListener(view -> finish());
        findViewById(R.id.btn_back).setOnClickListener(view -> finish());


        bntDownload1 = findViewById(R.id.tv_download_1);
        tvChargeNotice = findViewById(R.id.tv_charge_notice);
        btnBuyBook = findViewById(R.id.tv_buy_book);
        recyclerViewBooks = findViewById(R.id.recycler_view_books);
        btnPreview = findViewById(R.id.tv_buy_preview);
        btnDownload = findViewById(R.id.tv_download);


        tvDownloadNotice = findViewById(R.id.tv_download_notice);

        onBoardingBottomSheet = findViewById(R.id.bottom_sheet_buy_book);

        //sheetBehavior = BottomSheetBehavior.from(onBoardingBottomSheet);

        /*sheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull @NotNull View bottomSheet, int newState) {
                if(newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    viewFlipperMain.setDisplayedChild(0);
                    viewFlipperDownload.setDisplayedChild(0);
                    viewFlipperBuy.setDisplayedChild(0);
                }
            }

            @Override
            public void onSlide(@NonNull @NotNull View bottomSheet, float slideOffset) {

            }
        });
        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
*/
        btnBuyBook.setOnClickListener(view -> {
            viewFlipperBuy.showNext();
        });

        btnDownload.setOnClickListener(view -> {
            CoordinatorLayout.LayoutParams params = new CoordinatorLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,convertDpToPixelInt(270,this));
            params.gravity = Gravity.BOTTOM;
            params.setBehavior(new HideBottomViewOnScrollBehavior<View>());

            onBoardingBottomSheet.setLayoutParams(params);
            viewFlipperDownload.showNext();

            //sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        });

        findViewById(R.id.tv_buy_book_1).setOnClickListener(view -> {
            viewFlipperMain.showNext();
        });
        bntDownload1.setOnClickListener(view -> {
           // sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            CoordinatorLayout.LayoutParams params = new CoordinatorLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,convertDpToPixelInt(160,this));
            params.gravity = Gravity.BOTTOM;
            params.setBehavior(new HideBottomViewOnScrollBehavior<View>());
            onBoardingBottomSheet.setLayoutParams(params);
            viewFlipperMain.showNext();
            viewFlipperDownload.showNext();
            viewFlipperBuy.showNext();
        });
        books = new ArrayList<>();
        books.add(new Book(1, "Communication Skills for Dubottom_sheet_buy_bookmmies","Elizabeth Kuhnke",R.drawable.com_skills));
        books.add(new Book(2, "Complete ATLAS","DK Publication",R.drawable.complete_atlas));
        books.add(new Book(3, "Half of a Yellow Sun", "Chimamanda Ngozi A.",R.drawable.half_of_yellow_big));


        recyclerViewBooks.setNestedScrollingEnabled(false);
        //recyclerViewBooks.setHasFixedSize(true);

        booksAdapter = new HorizontalBooksAdapter(getApplicationContext(),this,books);
        recyclerViewBooks.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL, false));
        recyclerViewBooks.setAdapter(booksAdapter);

    }

    @Override
    public void onBookSelected(Book book) {
        startActivity(new Intent(this, BookInfoActivity.class));
    }
}