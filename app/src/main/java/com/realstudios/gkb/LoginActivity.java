package com.realstudios.gkb;

import android.os.Bundle;
import android.text.Html;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;

public class LoginActivity extends AppCompatActivity {

    AppCompatTextView tvAgree;
    String styledText = "I agree to <u><font color='#3594F9'>terms of using</font></u> this platform";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tvAgree = findViewById(R.id.tv_agree);

        tvAgree.setText(Utils.fromHtml(styledText));


    }
}
