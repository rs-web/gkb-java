package com.realstudios.gkb;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.realstudios.gkb.adapter.MainFragmentsAdapter;
import com.realstudios.gkb.databinding.ActivityMainBinding;
import com.realstudios.gkb.databinding.ActivityPlayListBinding;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;

public class MainActivity extends AppCompatActivity {
    public static final boolean DEBUG = true;
    private static final String TAG = MainActivity.class.getSimpleName();

    ActivityMainBinding binding;
   // AppCompatImageButton btnFabBG;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
       // btnFabBG = findViewById(R.id.btn_fab_bg);
        setContentView(binding.getRoot());

        final MainFragmentsAdapter adapter = new MainFragmentsAdapter(getSupportFragmentManager(),getLifecycle());
        binding.viewPager.setUserInputEnabled(false);
        binding.viewPager.setAdapter(adapter);


       setOnClickListener();

        KeyboardVisibilityEvent.setEventListener(
                this,
                this,
                isOpen -> {
                  if(isOpen){
                    //  bottomAppBar.performHide();
                      binding.fab.hide();
                      binding.fbMain.hide();
                      //btnFabBG.setVisibility(View.GONE);
                  }
                  else {
                      //bottomAppBar.performShow();
                      binding.fab.show();
                      binding.fbMain.show();
                     // btnFabBG.setVisibility(View.VISIBLE);
                  }
                });
    }
    public void setOnClickListener(){

        binding.fbMain.setOnClickListener(view -> startActivity(new Intent(this, ContinueActivity.class)));


        binding.viewHomeFragment.setOnClickListener(v -> {
            binding.viewPager.setCurrentItem(0);
            binding.btnHomeFragment.setImageResource(R.drawable.ic_home_selected);
            binding.btnLibraryFragment.setImageResource(R.drawable.ic_my_library_unselected);
            binding.btnBookmarkFragment.setImageResource(R.drawable.ic_bookmark_unselected);
            binding.btnNotesFragment.setImageResource(R.drawable.ic_notes_unselected);

            binding.tvHomeFragment.setTextColor(getColor(R.color.selectedTab));
            binding.tvLibraryFragment.setTextColor(getColor(R.color.unselectedTab));
            binding.tvBookmarkFragment.setTextColor(getColor(R.color.unselectedTab));
            binding.tvNotesFragment.setTextColor(getColor(R.color.unselectedTab));
            //navController.navigate(R.id.navigation_home);

        });
        binding.viewLibraryFragment.setOnClickListener(v -> {
            binding.viewPager.setCurrentItem(1);
            binding.btnHomeFragment.setImageResource(R.drawable.ic_home_unselected);
            binding.btnLibraryFragment.setImageResource(R.drawable.ic_my_library_selected);
            binding.btnBookmarkFragment.setImageResource(R.drawable.ic_bookmark_unselected);
            binding.btnNotesFragment.setImageResource(R.drawable.ic_notes_unselected);


            binding.tvHomeFragment.setTextColor(getColor(R.color.unselectedTab));
            binding.tvLibraryFragment.setTextColor(getColor(R.color.selectedTab));
            binding.tvBookmarkFragment.setTextColor(getColor(R.color.unselectedTab));
            binding.tvNotesFragment.setTextColor(getColor(R.color.unselectedTab));
            // navController.navigate(R.id.navigation_my_library);

        });
        binding.viewBookmarkFragment.setOnClickListener(v -> {
            binding.viewPager.setCurrentItem(2, false);
            binding.btnHomeFragment.setImageResource(R.drawable.ic_home_unselected);
            binding.btnLibraryFragment.setImageResource(R.drawable.ic_my_library_unselected);
            binding.btnBookmarkFragment.setImageResource(R.drawable.ic_book_mark_selected);
            binding.btnNotesFragment.setImageResource(R.drawable.ic_notes_unselected);


            binding.tvHomeFragment.setTextColor(getColor(R.color.unselectedTab));
            binding.tvLibraryFragment.setTextColor(getColor(R.color.unselectedTab));
            binding.tvBookmarkFragment.setTextColor(getColor(R.color.selectedTab));
            binding.tvNotesFragment.setTextColor(getColor(R.color.unselectedTab));

            //navController.navigate(R.id.navigation_my_bookmarks);
        });
        binding.viewNotesFragment.setOnClickListener(v -> {
            binding.viewPager.setCurrentItem(3);
            binding.btnHomeFragment.setImageResource(R.drawable.ic_home_unselected);
            binding.btnLibraryFragment.setImageResource(R.drawable.ic_my_library_unselected);
            binding.btnBookmarkFragment.setImageResource(R.drawable.ic_bookmark_unselected);
            binding.btnNotesFragment.setImageResource(R.drawable.ic_my_notes_selected);

            binding.tvHomeFragment.setTextColor(getColor(R.color.unselectedTab));
            binding.tvLibraryFragment.setTextColor(getColor(R.color.unselectedTab));
            binding.tvBookmarkFragment.setTextColor(getColor(R.color.unselectedTab));
            binding.tvNotesFragment.setTextColor(getColor(R.color.selectedTab));
            //navController.navigate(R.id.navigation_my_notes);
        });


        binding.btnHomeFragment.setOnClickListener(v -> {
            binding.viewPager.setCurrentItem(0);
            binding.btnHomeFragment.setImageResource(R.drawable.ic_home_selected);
            binding.btnLibraryFragment.setImageResource(R.drawable.ic_my_library_unselected);
            binding.btnBookmarkFragment.setImageResource(R.drawable.ic_bookmark_unselected);
            binding.btnNotesFragment.setImageResource(R.drawable.ic_notes_unselected);

            binding.tvHomeFragment.setTextColor(getColor(R.color.selectedTab));
            binding.tvLibraryFragment.setTextColor(getColor(R.color.unselectedTab));
            binding.tvBookmarkFragment.setTextColor(getColor(R.color.unselectedTab));
            binding.tvNotesFragment.setTextColor(getColor(R.color.unselectedTab));
            //navController.navigate(R.id.navigation_home);

        });
        binding.btnLibraryFragment.setOnClickListener(v -> {
            binding.viewPager.setCurrentItem(1);
            binding.btnHomeFragment.setImageResource(R.drawable.ic_home_unselected);
            binding.btnLibraryFragment.setImageResource(R.drawable.ic_my_library_selected);
            binding.btnBookmarkFragment.setImageResource(R.drawable.ic_bookmark_unselected);
            binding.btnNotesFragment.setImageResource(R.drawable.ic_notes_unselected);

            binding.tvHomeFragment.setTextColor(getColor(R.color.unselectedTab));
            binding.tvLibraryFragment.setTextColor(getColor(R.color.selectedTab));
            binding.tvBookmarkFragment.setTextColor(getColor(R.color.unselectedTab));
            binding.tvNotesFragment.setTextColor(getColor(R.color.unselectedTab));
            // navController.navigate(R.id.navigation_my_library);

        });
        binding.btnBookmarkFragment.setOnClickListener(v -> {
            binding.viewPager.setCurrentItem(2, false);
            binding.btnHomeFragment.setImageResource(R.drawable.ic_home_unselected);
            binding.btnLibraryFragment.setImageResource(R.drawable.ic_my_library_unselected);
            binding.btnBookmarkFragment.setImageResource(R.drawable.ic_book_mark_selected);
            binding.btnNotesFragment.setImageResource(R.drawable.ic_notes_unselected);

            binding.tvHomeFragment.setTextColor(getColor(R.color.unselectedTab));
            binding.tvLibraryFragment.setTextColor(getColor(R.color.unselectedTab));
            binding.tvBookmarkFragment.setTextColor(getColor(R.color.selectedTab));
            binding.tvNotesFragment.setTextColor(getColor(R.color.unselectedTab));
            //navController.navigate(R.id.navigation_my_bookmarks);
        });
        binding.btnNotesFragment.setOnClickListener(v -> {
            binding.viewPager.setCurrentItem(3);
            binding.btnHomeFragment.setImageResource(R.drawable.ic_home_unselected);
            binding.btnLibraryFragment.setImageResource(R.drawable.ic_my_library_unselected);
            binding.btnBookmarkFragment.setImageResource(R.drawable.ic_bookmark_unselected);
            binding.btnNotesFragment.setImageResource(R.drawable.ic_my_notes_selected);


            binding.tvHomeFragment.setTextColor(getColor(R.color.unselectedTab));
            binding.tvLibraryFragment.setTextColor(getColor(R.color.unselectedTab));
            binding.tvBookmarkFragment.setTextColor(getColor(R.color.unselectedTab));
            binding.tvNotesFragment.setTextColor(getColor(R.color.selectedTab));
            //navController.navigate(R.id.navigation_my_notes);
        });


    }
}