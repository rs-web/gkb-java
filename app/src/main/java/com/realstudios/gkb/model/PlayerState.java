package com.realstudios.gkb.model;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class PlayerState {

    public final String playerState;

    // ... type definitions
    // Describes when the annotation will be discarded
    @Retention(RetentionPolicy.SOURCE)
    // Enumerate valid values for this interface
    @StringDef({ PLAYING,PAUSED,QUEUED,ERROR})
    // Create an interface for validating String types
    public @interface FilterPlayerStateDef {}
    // Declare the constants
    public static final String PLAYING = "Playing";
    public static final String PAUSED = "Paused";
    public static final String QUEUED = "Queued";
    public static final String ERROR = "Error";

    // Mark the argument as restricted to these enumerated types
    public PlayerState(@FilterPlayerStateDef String playerState) {
        this.playerState = playerState;
    }
}
