package com.realstudios.gkb.model;

import java.util.ArrayList;
import java.util.List;

public class Book {
    private int id;
    private String title;
    private String author;
    private int bookCover;
    private String bookType;
    private List<BookPage> bookPages;
    private BookPage currentBookmarkPage;

    public BookPage getCurrentBookmarkPage() {
        return currentBookmarkPage;
    }

    public void setCurrentBookmarkPage(BookPage currentBookmarkPage) {
        this.currentBookmarkPage = currentBookmarkPage;
    }

    public String getBookType() {
        return bookType;
    }

    public void setBookType(String bookType) {
        this.bookType = bookType;
    }

    public List<BookPage> getBookPages() {
        return bookPages;
    }

    public void setBookPages(List<BookPage> bookPages) {
        this.bookPages = bookPages;
    }


    public Book(int id, String title, String author, int bookCover, String bookType) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.bookCover = bookCover;
        this.bookType = bookType;
    }

    public Book(int id, String title, String author, int bookCover, List<BookPage> bookPages) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.bookCover = bookCover;
        this.bookPages = bookPages;
        currentBookmarkPage = bookPages.get(1);

    }
    public Book(int id, String title, String author, int bookCover) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.bookCover = bookCover;
        this.bookPages = new ArrayList<>();
        bookPages.add(new BookPage(1, "Chapter One",1, 1));
        bookPages.add(new BookPage(2, "Chapter Two",2, 1));
        bookPages.add(new BookPage(3, "Chapter Three",3, 1));
        bookPages.add(new BookPage(4, "Chapter Four",4, 1));
        bookPages.add(new BookPage(5, "Chapter Five",5, 1));
        bookPages.add(new BookPage(6, "Chapter Six",6, 1));
        bookPages.add(new BookPage(7, "Chapter Seven",7, 1));
        bookPages.add(new BookPage(8, "Chapter Eight",8, 1));
        bookPages.add(new BookPage(10, "Chapter Seven",9, 1));
        currentBookmarkPage = bookPages.get(1);

    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getBookCover() {
        return bookCover;
    }

    public void setBookCover(int bookCover) {
        this.bookCover = bookCover;
    }
}
