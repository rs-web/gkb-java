package com.realstudios.gkb.model;

public class Download {
    private int id;
    private Book book;
    private double percentage;
    private String status;


    public Download(int id, Book book, double percentage, String status) {
        this.id = id;
        this.book = book;
        this.percentage = percentage;
        this.status = status;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
