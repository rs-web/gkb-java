package com.realstudios.gkb.model;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class DownloadState {

    public final String downloadState;

    // ... type definitions
    // Describes when the annotation will be discarded
    @Retention(RetentionPolicy.SOURCE)
    // Enumerate valid values for this interface
    @StringDef({ FAILED,COMPLETED,DOWNLOADING,QUEUED})
    // Create an interface for validating String types
    public @interface FilterDownloadStateDef {}
    // Declare the constants
    public static final String FAILED = "Failed";
    public static final String COMPLETED = "Completed";
    public static final String DOWNLOADING = "Downloading";
    public static final String QUEUED = "Queued";



    // Mark the argument as restricted to these enumerated types
    public DownloadState(@FilterDownloadStateDef String downloadState) {
        this.downloadState = downloadState;
    }
}
