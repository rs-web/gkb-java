package com.realstudios.gkb.model;

public class BookPage {

    private int id;
    private String chapterTitle;
    private int pageNumber;
    private int bookId;
    private boolean isCurrent;

    public boolean isCurrent() {
        return isCurrent;
    }

    public void setCurrent(boolean current) {
        isCurrent = current;
    }

    public BookPage(int id, String chapterTitle, int pageNumber, int bookId) {
        this.id = id;
        this.chapterTitle = chapterTitle;
        this.pageNumber = pageNumber;
        this.bookId = bookId;
    }

    public BookPage(int id, String chapterTitle, int pageNumber, int bookId, boolean isCurrent) {
        this.id = id;
        this.chapterTitle = chapterTitle;
        this.pageNumber = pageNumber;
        this.bookId = bookId;
        this.isCurrent = isCurrent;
    }

    public BookPage() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getChapterTitle() {
        return chapterTitle;
    }

    public void setChapterTitle(String chapterTitle) {
        this.chapterTitle = chapterTitle;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }
}
