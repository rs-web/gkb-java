package com.realstudios.gkb.model;

public class VideoObject {
    private String title;
    private String source;
    private String image;


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public VideoObject(String title, String source, String image) {
        this.title = title;
        this.source = source;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
