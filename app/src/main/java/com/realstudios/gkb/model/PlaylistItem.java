package com.realstudios.gkb.model;

public class PlaylistItem {
    private int id;
    private String state;
    private String title;
    private String description;
    private String type;


    public PlaylistItem(int id, String state, String title, String description, String type) {
        this.id = id;
        this.state = state;
        this.title = title;
        this.description = description;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
