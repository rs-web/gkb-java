package com.realstudios.gkb.model;

public class Reading {
    private int id;
    private Book book;
    private double percentage;


    public Reading(int id, Book book, double percentage) {
        this.id = id;
        this.book = book;
        this.percentage = percentage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }
}
