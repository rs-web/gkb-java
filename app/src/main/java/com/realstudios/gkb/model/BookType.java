package com.realstudios.gkb.model;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class BookType {

    public final String bookType;

    // ... type definitions
    // Describes when the annotation will be discarded
    @Retention(RetentionPolicy.SOURCE)
    // Enumerate valid values for this interface
    @StringDef({ BOOK,AUDIOBOOK,ARTICLE,VIDEO, FAVORITE, BOOKMARK, HISTORY, RECENTLY_ADDED, FEATURED, POPULAR_FILES})
    // Create an interface for validating String types
    public @interface FilterBookTypeDef {}
    // Declare the constants
    public static final String BOOK = "Book";
    public static final String AUDIOBOOK = "Audiobook";
    public static final String ARTICLE = "Article";
    public static final String VIDEO = "Video";
    public static final String FAVORITE = "Favorite";
    public static final String BOOKMARK = "Bookmark";
    public static final String HISTORY = "History";

    public static final String RECENTLY_ADDED = "RecentlyAdded";
    public static final String FEATURED = "Featured";
    public static final String POPULAR_FILES = "Popular files";


    // Mark the argument as restricted to these enumerated types
    public BookType(@FilterBookTypeDef String bookType) {
        this.bookType = bookType;
    }
}
