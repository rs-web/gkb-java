package com.realstudios.gkb.model;

public class Avatar {
    private int id;
    private int icon;
    private boolean isSelected;

    public Avatar(int id, int icon, boolean isSelected) {
        this.id = id;
        this.icon = icon;
        this.isSelected = isSelected;
    }

    public Avatar(int id, int icon) {
        this.id = id;
        this.icon = icon;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
