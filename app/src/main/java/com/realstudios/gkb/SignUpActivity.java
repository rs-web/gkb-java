package com.realstudios.gkb;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;

@SuppressLint("ClickableViewAccessibility")
public class SignUpActivity extends AppCompatActivity {


    AppCompatTextView tvAgree;
    AutoCompleteTextView autIDType;
    String styledText = "I agree to <u><font color='#3594F9'>terms of using</font></u> this platform";
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        tvAgree = findViewById(R.id.tv_agree);

        autIDType = findViewById(R.id.edt_id_type);

        tvAgree.setText(Utils.fromHtml(styledText));


        autIDType.setOnTouchListener((View v,  MotionEvent event) -> {
            final int DRAWABLE_RIGHT = 2;
            if(event.getAction() == MotionEvent.ACTION_UP) {
                if(event.getRawX() >= (autIDType.getRight() - autIDType.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {

                    Toast.makeText(this, "search drop down clicked", Toast.LENGTH_SHORT).show();
                    return true;
                }
            }
            return false;
        });

        findViewById(R.id.tv_send_verification).setOnClickListener(v -> {
            startActivity(new Intent(this, VerificationActivity.class));
            finish();
        });

    }
}
