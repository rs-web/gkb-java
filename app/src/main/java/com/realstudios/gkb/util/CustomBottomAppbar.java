package com.realstudios.gkb.util;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomappbar.BottomAppBar;

import org.jetbrains.annotations.NotNull;

import static com.realstudios.gkb.Utils.RoundedRect;

public class CustomBottomAppbar extends BottomAppBar {
    private Path mPath;
    private Paint mPaint;
    private float mNavigationBarWidth;
    private float mNavigationBarHeight;

    public CustomBottomAppbar(@NonNull @NotNull Context context,
                              @Nullable @org.jetbrains.annotations.Nullable AttributeSet attrs,
                              int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mNavigationBarWidth = getWidth();
        mNavigationBarHeight = getHeight();
        mPath = new Path();
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaint.setColor(Color.WHITE);
        setBackgroundColor(Color.TRANSPARENT);
    }

    public CustomBottomAppbar(@NonNull @NotNull Context context,
                              @Nullable @org.jetbrains.annotations.Nullable AttributeSet attrs) {
        super(context, attrs);

        mNavigationBarWidth = getWidth();
        mNavigationBarHeight = getHeight();

        mPath = new Path();
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaint.setColor(Color.WHITE);
        setBackgroundColor(Color.TRANSPARENT);
    }

    public CustomBottomAppbar(@NonNull @NotNull Context context) {
        super(context);
        mNavigationBarWidth = getWidth();
        mNavigationBarHeight = getHeight();
        mPath = new Path();
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaint.setColor(Color.WHITE);
        setBackgroundColor(Color.TRANSPARENT);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mNavigationBarWidth = getWidth();
        mNavigationBarHeight = getHeight();
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mPath.reset();
        mPath = RoundedRect(0, 0, mNavigationBarWidth, mNavigationBarHeight, 50, 50, true, true, false, false);
        canvas.drawPath(mPath, mPaint);
    }
}
