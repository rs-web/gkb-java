/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.realstudios.gkb.util;

import android.content.Context;
import android.security.keystore.KeyGenParameterSpec;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;
import androidx.security.crypto.EncryptedFile;
import androidx.security.crypto.MasterKeys;

import com.realstudios.gkb.GKBApp;
import com.tonyodev.fetch2.Download;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.security.GeneralSecurityException;

import kotlin.io.ByteStreamsKt;

import static com.realstudios.gkb.Utils.getNameFromUrl;
import static com.realstudios.gkb.Utils.getSaveDir;
import static com.realstudios.gkb.Utils.readBytes;


final class WorkerUtils {
    private static final String TAG = WorkerUtils.class.getSimpleName();


    @WorkerThread
    public static boolean readAndWriteFromFile(@NonNull String downloadFile, @NonNull String downloadUrl, Context context) throws IOException, GeneralSecurityException {
        KeyGenParameterSpec keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC;
        String mainKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec);
        File oldFile = new File(downloadFile);
        InputStream inputStream = new FileInputStream((oldFile));
        String fileName = getNameFromUrl(downloadUrl);
        File newFile = new File(getSaveDir(context,"GKB"), fileName);

        byte [] bytes = ByteStreamsKt.readBytes(inputStream);
        // if File doesnt exists, then create it

        if (!newFile.getParentFile().exists())
            newFile.getParentFile().mkdirs();

        /*if (!newFile.exists()) {

            newFile.createNewFile();
        }*/

        EncryptedFile encryptedFile = new EncryptedFile.Builder(
                newFile,
                context,
                mainKeyAlias,
                EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB
        ).build();

        OutputStream outputStream = encryptedFile.openFileOutput();
        outputStream.write(bytes);
        outputStream.flush();
        outputStream.close();

        decrypt(getSaveDir(context,"GKB"),fileName,context);
        return true;
    }



    @WorkerThread
    public static boolean decrypt(String directory, String fileName,Context context) throws IOException, GeneralSecurityException {
        File tempFile = File.createTempFile("GKB-", ".mp4");
        //File newFile = new File(getSaveDir(GKBApp.getApp(),"GKB"), UUID.randomUUID().toString()+""+fileName);
        //File tempFile = File.createTempFile("MyAppName-", ".tmp");
        //tempFile.deleteOnExit();

        KeyGenParameterSpec keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC;
        String mainKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec);

        EncryptedFile encryptedFile = new EncryptedFile.Builder(
                new File(directory, fileName),
                context,
                mainKeyAlias,
                EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB
        ).build();

        InputStream inputStream = encryptedFile.openFileInput();
        copyInputStreamToFile(inputStream, tempFile);

        return true;
        //Log.d("decrypt",);
    }
    private static void copyInputStreamToFile(InputStream in, File file) {
        OutputStream out = null;

        try {
            out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while((len=in.read(buf))>0){
                out.write(buf,0,len);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            // Ensure that the InputStreams are closed even if there's an exception.
            try {
                if ( out != null ) {
                    out.close();
                }

                // If you want to close the "in" InputStream yourself then remove this
                // from here but ensure that you close it yourself eventually.
                in.close();
            }
            catch ( IOException e ) {
                e.printStackTrace();
            }
        }
        Log.d("copyInputStreamToFile", "file path="+file.getAbsolutePath());
    }

    /**
     * Encrypts the given Download file
     * @param downloadFile Download to encrypt
     * @param applicationContext Application context
     * @return success result
     */
    @WorkerThread
    public static boolean encryptFile(@NonNull String downloadFile, @NonNull String downloadUrl,
                             @NonNull Context applicationContext) throws GeneralSecurityException, IOException {


            KeyGenParameterSpec keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC;
            String mainKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec);
            File oldFile = new File(downloadFile);
            InputStream inputStream = new FileInputStream((oldFile));
            String fileName = getNameFromUrl(downloadUrl);
            File newFile = new File(getSaveDir(applicationContext, "GKB"), fileName);

            byte[] bytes = readBytes(inputStream);
            // if File doesnt exists, then create it

            if (!newFile.getParentFile().exists())
                newFile.getParentFile().mkdirs();

        /*if (!newFile.exists()) {

            newFile.createNewFile();
        }*/


            EncryptedFile encryptedFile = new EncryptedFile.Builder(
                    newFile,
                    applicationContext,
                    mainKeyAlias,
                    EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB
            ).build();

            OutputStream outputStream = encryptedFile.openFileOutput();
            outputStream.write(bytes);
            outputStream.flush();
            outputStream.close();
            return true;

    }


    private WorkerUtils() {
    }
}