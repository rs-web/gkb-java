package com.realstudios.gkb.util

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.jakewharton.rxbinding4.view.clicks
import com.realstudios.gkb.MainActivity
import com.realstudios.gkb.R
import com.realstudios.gkb.util.ktx.animate
import com.realstudios.gkb.util.ktx.isInterruptedCaused
import com.realstudios.gkb.util.ktx.isNetworkRelated
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import org.schabi.newpipe.extractor.exceptions.AccountTerminatedException
import org.schabi.newpipe.extractor.exceptions.AgeRestrictedContentException
import org.schabi.newpipe.extractor.exceptions.ContentNotAvailableException
import org.schabi.newpipe.extractor.exceptions.ContentNotSupportedException
import org.schabi.newpipe.extractor.exceptions.GeographicRestrictionException
import org.schabi.newpipe.extractor.exceptions.PaidContentException
import org.schabi.newpipe.extractor.exceptions.PrivateContentException
import org.schabi.newpipe.extractor.exceptions.ReCaptchaException
import org.schabi.newpipe.extractor.exceptions.SoundCloudGoPlusContentException
import org.schabi.newpipe.extractor.exceptions.YoutubeMusicPremiumContentException
import org.schabi.newpipe.extractor.utils.Utils.isNullOrEmpty
import java.util.concurrent.TimeUnit

class ErrorPanelHelper(
    private val fragment: Fragment,
    rootView: View,
    onRetry: Runnable
) {
    private val context: Context = rootView.context!!

    private val errorPanelRoot: View = rootView.findViewById(R.id.error_panel)

    // the only element that is visible by default
    private val errorTextView: TextView =
        errorPanelRoot.findViewById(R.id.error_message_view)
    private val errorServiceInfoTextView: TextView =
        errorPanelRoot.findViewById(R.id.error_message_service_info_view)
    private val errorServiceExplanationTextView: TextView =
        errorPanelRoot.findViewById(R.id.error_message_service_explanation_view)
    private val errorActionButton: Button =
        errorPanelRoot.findViewById(R.id.error_action_button)
    private val errorRetryButton: Button =
        errorPanelRoot.findViewById(R.id.error_retry_button)

    private var errorDisposable: Disposable? = null

    init {
        errorDisposable = errorRetryButton.clicks()
            .debounce(300, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { onRetry.run() }
    }

    private fun ensureDefaultVisibility() {
        errorTextView.isVisible = true

        errorServiceInfoTextView.isVisible = false
        errorServiceExplanationTextView.isVisible = false
        errorActionButton.isVisible = false
        errorRetryButton.isVisible = false
    }

    fun showError(errorInfo: ErrorInfo) {

        if (errorInfo.throwable != null && errorInfo.throwable!!.isInterruptedCaused) {
            if (DEBUG) {
                Log.w(TAG, "onError() isInterruptedCaused! = [$errorInfo.throwable]")
            }
            return
        }

        ensureDefaultVisibility()

        if (errorInfo.throwable is ReCaptchaException) {
            errorTextView.setText(R.string.recaptcha_request_toast)

            showAndSetErrorButtonAction(
                R.string.recaptcha_solve
            ) {
                // Starting ReCaptcha Challenge Activity

                errorActionButton.setOnClickListener(null)
            }

            errorRetryButton.isVisible = true
        } else if (errorInfo.throwable is AccountTerminatedException) {
            errorTextView.setText(R.string.account_terminated)

            if (!isNullOrEmpty((errorInfo.throwable as AccountTerminatedException).message)) {
                errorServiceInfoTextView.text = context.resources.getString(
                    R.string.service_provides_reason,"GKB")
                errorServiceInfoTextView.isVisible = true

                errorServiceExplanationTextView.text =
                    (errorInfo.throwable as AccountTerminatedException).message
                errorServiceExplanationTextView.isVisible = true
            }
        } else {
            showAndSetErrorButtonAction(
                R.string.error_snackbar_action
            ) {
               // ErrorActivity.reportError(context, errorInfo)
            }

            errorTextView.setText(
                when (errorInfo.throwable) {
                    is ContentNotAvailableException -> R.string.content_not_available
                    is ContentNotSupportedException -> R.string.content_not_supported
                    else -> {
                        // show retry button only for content which is not unavailable or unsupported
                        errorRetryButton.isVisible = true
                        if (errorInfo.throwable != null && errorInfo.throwable!!.isNetworkRelated) {
                            R.string.network_error
                        } else {
                            R.string.error_snackbar_message
                        }
                    }
                }
            )
        }

        setRootVisible()
    }

    /**
     * Shows the errorButtonAction, sets a text into it and sets the click listener.
     */
    private fun showAndSetErrorButtonAction(
        @StringRes resid: Int,
        @Nullable listener: View.OnClickListener
    ) {
        errorActionButton.isVisible = true
        errorActionButton.setText(resid)
        errorActionButton.setOnClickListener(listener)
    }

    fun showTextError(errorString: String) {
        ensureDefaultVisibility()

        errorTextView.text = errorString

        setRootVisible()
    }

    private fun setRootVisible() {
        errorPanelRoot.animate(true, 300)
    }

    fun hide() {
        errorActionButton.setOnClickListener(null)
        errorPanelRoot.animate(false, 150)
    }

    fun isVisible(): Boolean {
        return errorPanelRoot.isVisible
    }

    fun dispose() {
        errorActionButton.setOnClickListener(null)
        errorRetryButton.setOnClickListener(null)
        errorDisposable?.dispose()
    }

    companion object {
        val TAG: String = ErrorPanelHelper::class.simpleName!!
        val DEBUG: Boolean = MainActivity.DEBUG
    }
}
