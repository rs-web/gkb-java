package com.realstudios.gkb.util;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.google.gson.Gson;
import com.tonyodev.fetch2.Download;

import org.jetbrains.annotations.NotNull;


public class FileEncryptionWorker extends Worker {

    public FileEncryptionWorker(@NonNull @NotNull Context context,
                                @NonNull @NotNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @NotNull
    @Override
    public Result doWork() {
        boolean result = false;
        String downloadedFile = getInputData().getString("downloaded_file");
        String downloadedUrl = getInputData().getString("downloaded_url");

        Log.d("doWork", "downloaded_file:"+downloadedFile);
        Data outputData;
        try {
            result = WorkerUtils.encryptFile(downloadedFile,downloadedUrl,getApplicationContext());

            outputData = new Data.Builder()
                    .putBoolean("encryption_result", result)
                    .build();
            Log.d("doWork", "result:"+result);
            return Result.success(outputData);
        }
        catch (Exception e){
            e.printStackTrace();
            outputData = new Data.Builder()
                    .putBoolean("encryption_result", result)
                    .build();
            Log.d("doWork", "result:"+result);
            return Result.failure(outputData);
        }
    }
}
