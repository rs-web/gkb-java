package com.realstudios.gkb;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

public class VerificationActivity extends AppCompatActivity {

    AppCompatTextView tvWrongNumber,tvDidntReceiveCode,tvInvalidCode;

    AppCompatEditText edtVerificationCode;
    String wrongNumber = "Wrong number?<br><u><font color='#3594F9'>Click to change number</font></u>";
    String didntReceiveCode = "Didn’t receive code after 5 minutes?<br><u><font color='#3594F9'>Click here to resend code</font></u>";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        tvDidntReceiveCode = findViewById(R.id.tv_didnt_receive_code);
        tvWrongNumber = findViewById(R.id.tv_wrong_number);
        tvInvalidCode = findViewById(R.id.tv_invalid_code);
        tvWrongNumber.setText(Utils.fromHtml(wrongNumber));
        edtVerificationCode = findViewById(R.id.edt_verification_code);
        tvDidntReceiveCode.setText(Utils.fromHtml(didntReceiveCode));

        edtVerificationCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.toString().trim().length()>3){
                    tvInvalidCode.setVisibility(View.INVISIBLE);

                }
                else {
                    tvInvalidCode.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        findViewById(R.id.tv_confirm_verification_code).setOnClickListener(v -> {
            if(edtVerificationCode.getText()!=null){
                if(edtVerificationCode.getText().toString().trim().length()<=3){
                  tvInvalidCode.setVisibility(View.VISIBLE);
                }
                else {
                    tvInvalidCode.setVisibility(View.INVISIBLE);
                    if(edtVerificationCode.getText().toString().equals("54321")) {
                        startActivity(new Intent(this, ActivationSuccessfulActivity.class));
                        finish();
                    }
                    else {
                        tvInvalidCode.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        //Didn’t I receive you code after 5 minutes
        //Click here to resend code

    }
}
