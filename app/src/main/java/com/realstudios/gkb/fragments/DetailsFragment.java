package com.realstudios.gkb.fragments;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.realstudios.gkb.databinding.FragmentDetailsBinding;

import org.jetbrains.annotations.NotNull;


public class DetailsFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    FragmentDetailsBinding binding;


    public DetailsFragment() {
    }


    public static DetailsFragment newInstance(String param1, String param2) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentDetailsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }


    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        String title = "Python for Data Science";
        binding.tvTitle.setText(title);

        binding.tvSubject.setText(Html.fromHtml(createText("Subject:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;","Core Mathematics")));
        binding.tvTopic.setText(Html.fromHtml(createText("Topic:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "Trigonometry")));
        binding.tvAuthor.setText(Html.fromHtml(createText("Author:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "Mr Osei Owusu")));
        binding.tvCopyright.setText(Html.fromHtml(createText("Copyright:&nbsp;&nbsp;", "2020")));
        binding.tvFileSize.setText(Html.fromHtml(createText("File size:&nbsp;&nbsp;&nbsp;&nbsp;", "300MB")));
        binding.tvDuration.setText(Html.fromHtml(createText("Duration:&nbsp;&nbsp;&nbsp;", "1hr 30min")));
        binding.tvLanguage.setText(Html.fromHtml(createText("Language:&nbsp;&nbsp;", "English")));
        binding.tvGenre.setText(Html.fromHtml(createText("Genre:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "Biography")));
        binding.tvFile.setText(Html.fromHtml(createText("File:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "CID546463987")));

    }

    private String createText(String title, String details){
        return "<font color=#777777>"+title+"</font><font color=#314052>"+details+"</font>";
    }
}