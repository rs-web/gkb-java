package com.realstudios.gkb.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.realstudios.gkb.R;
import com.realstudios.gkb.adapter.MyNotesAdapter;
import com.realstudios.gkb.model.Note;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;


public class NoteFragment extends Fragment implements MyNotesAdapter.setOnNoteListener{



    MyNotesAdapter notesAdapter;
    RecyclerView recyclerView;
    List<Note> notes;

    public NoteFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_note, container, false);
    }


    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.recycler_view_notes);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setNestedScrollingEnabled(false);
        notes = new ArrayList<>();
        notesAdapter = new MyNotesAdapter(getContext(),this, getNotes());
        recyclerView.setAdapter(notesAdapter);
    }

    public List<Note> getNotes() {
        for(int i = 1; i <=20; i++){
            notes.add(new Note(i, "Note heading "+i, "The quick brown fox jumped over the lazy dog"));
        }
        return notes;
    }

    @Override
    public void onNoteSelected(Note note) {

    }
}