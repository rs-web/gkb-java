package com.realstudios.gkb.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.realstudios.gkb.BookInfoActivity;
import com.realstudios.gkb.R;
import com.realstudios.gkb.adapter.HorizontalBooksAdapter;
import com.realstudios.gkb.model.Book;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;


public class HomeItemFragment extends Fragment implements HorizontalBooksAdapter.setOnBookListener{


    RecyclerView recyclerViewBooks,recyclerViewAudioBooks;
    HorizontalBooksAdapter booksAdapter,audiobooksAdapter;
    List<Book> books, audiobooks;
    private static final String BOOK_TYPE = "BOOK_TYPE";
    private String mBOOK_TYPE ="";
    public HomeItemFragment() {
        // Required empty public constructor
    }



    public static HomeItemFragment newInstance(String booKType) {
        HomeItemFragment fragment = new HomeItemFragment();
        Bundle args = new Bundle();
        args.putString(BOOK_TYPE,booKType );
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mBOOK_TYPE = getArguments().getString(BOOK_TYPE);
        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_item, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        recyclerViewBooks = view.findViewById(R.id.recycler_view_books);
        recyclerViewBooks.setNestedScrollingEnabled(false);
       // recyclerViewBooks.setHasFixedSize(true);
        recyclerViewAudioBooks = view.findViewById(R.id.recycler_view_audio_books);
        recyclerViewAudioBooks.setNestedScrollingEnabled(false);
       // recyclerViewAudioBooks.setHasFixedSize(true);
        books = new ArrayList<>();
        audiobooks = new ArrayList<>();

        books.add(new Book(1, "Ghana Constitution","Supreme Court Ghana",R.drawable.constitution));
        books.add(new Book(2, "King Peggy Eleanor","Herman & Peggielene Bartels",R.drawable.king_peggy));
        books.add(new Book(3, "Half of a Yellow Sun", "Chimamanda Ngozi A.",R.drawable.half_of_yellow_sun));



        audiobooks.add(new Book(1, "Persuasion IQ","Kurt W. Mortensen",R.drawable.persuasion_iq));
        audiobooks.add(new Book(2, "The AI Advantage","Thomas H. Davenport",R.drawable.ai_advantage));
        audiobooks.add(new Book(3, "Power VS Force", "David R. Hawkins PhD.",R.drawable.power_vs_force));


        booksAdapter = new HorizontalBooksAdapter(getContext(),this,books);
        recyclerViewBooks.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL, false));
        recyclerViewBooks.setAdapter(booksAdapter);

        audiobooksAdapter = new HorizontalBooksAdapter(getContext(),this,audiobooks);
        recyclerViewAudioBooks.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL, false));
        recyclerViewAudioBooks.setAdapter(audiobooksAdapter);
    }

    @Override
    public void onBookSelected(Book book) {
        startActivity(new Intent(getActivity(), BookInfoActivity.class));
    }
}