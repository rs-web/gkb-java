package com.realstudios.gkb.fragments;

import com.realstudios.gkb.model.Book;

import java.util.List;

public class BookList {
    private String bookType;
    private List<Book> books;

    public BookList(String bookType, List<Book> books) {
        this.bookType = bookType;
        this.books = books;
    }

    public String getBookType() {
        return bookType;
    }

    public void setBookType(String bookType) {
        this.bookType = bookType;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
}
