package com.realstudios.gkb.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.realstudios.gkb.R;
import com.realstudios.gkb.util.PageTransformer;
import com.vanniktech.emoji.EmojiPopup;

import org.jetbrains.annotations.NotNull;

public class NewNoteFragment extends Fragment {

    private static final String TAG =NewNoteFragment.class.getSimpleName() ;
    EmojiPopup emojiPopup;

    View addNotesRootView;
    AppCompatEditText edtNoteDetails;
    public NewNoteFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_note, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addNotesRootView = view.findViewById(R.id.view_add_note_root_view);
        edtNoteDetails = view.findViewById(R.id.edt_note);

        setUpEmojiPopup();

        view.findViewById(R.id.tv_emoji).setOnClickListener(ignore -> emojiPopup.toggle());
    }

    private void setUpEmojiPopup() {
        emojiPopup = EmojiPopup.Builder.fromRootView(addNotesRootView)
                .setOnEmojiBackspaceClickListener(ignore -> Log.d(TAG, "Clicked on Backspace"))
                .setOnEmojiClickListener((ignore, ignore2) -> Log.d(TAG, "Clicked on emoji"))
                .setOnEmojiPopupShownListener(() -> {
                    //emojiButton.setImageResource(R.drawable.ic_keyboard);
                })
                .setOnSoftKeyboardOpenListener(ignore -> Log.d(TAG, "Opened soft keyboard"))
                .setOnEmojiPopupDismissListener(() -> {
                   // emojiButton.setImageResource(R.drawable.emoji_ios_category_smileysandpeople);
                })
                .setOnSoftKeyboardCloseListener(() -> Log.d(TAG, "Closed soft keyboard"))
                .setKeyboardAnimationStyle(R.style.emoji_fade_animation_style)
                .setPageTransformer(new PageTransformer())
                .build(edtNoteDetails);
    }
}