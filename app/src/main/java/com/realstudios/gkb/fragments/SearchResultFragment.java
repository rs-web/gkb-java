package com.realstudios.gkb.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.realstudios.gkb.BookInfoActivity;
import com.realstudios.gkb.R;
import com.realstudios.gkb.adapter.BooksAdapter;
import com.realstudios.gkb.adapter.SearchResultAdapter;
import com.realstudios.gkb.model.Book;
import com.realstudios.gkb.model.BookType;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;


public class SearchResultFragment extends Fragment implements SearchResultAdapter.setOnBookListener{

    private static final String BOOK_TYPE = "BOOK_TYPE";
    private static final String SEARCH_STRING = "SEARCH_STRING";
    private String mBOOK_TYPE ="";
    private String mSearchString ="";
    SearchResultAdapter searchResultAdapter;
    ViewFlipper viewFlipper;
    RecyclerView recyclerViewBooks;
    List<Book> books;

    public SearchResultFragment() {
        // Required empty public constructor
    }

    public static SearchResultFragment newInstance(String booKType,String searchString) {
        SearchResultFragment fragment = new SearchResultFragment();
        Bundle args = new Bundle();
        args.putString(BOOK_TYPE,booKType);
        args.putString(SEARCH_STRING,searchString);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mBOOK_TYPE = getArguments().getString(BOOK_TYPE);
            mSearchString = getArguments().getString(SEARCH_STRING);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        viewFlipper = view.findViewById(R.id.view_flipper_search_result);

        recyclerViewBooks = view.findViewById(R.id.recycler_view_books);
        recyclerViewBooks.setNestedScrollingEnabled(false);
        recyclerViewBooks.setLayoutManager(new LinearLayoutManager(getContext()));
        //recyclerViewBooks.setHasFixedSize(true);

        books = new ArrayList<>();

        if(mSearchString.equalsIgnoreCase("none")){
            viewFlipper.setDisplayedChild(1);
        }
        else {
            viewFlipper.setDisplayedChild(0);
        }
        switch (mBOOK_TYPE){
            case BookType.BOOK:
                books.add(new Book(1, "Talk like TED", "Carmine Gallo", R.drawable.talk_like_ted));
                books.add(new Book(2, "Never Split the Difference", "Chris Voss", R.drawable.never_split_the_diff));
                books.add(new Book(3, "King Peggy", "Peggylin Bartels", R.drawable.king_peggy_big));
                books.add(new Book(4, "Think like Einstein", "Peter Hollins", R.drawable.think_like_einstein));
                books.add(new Book(5, "Will it Fly?", "Pat Flynn", R.drawable.will_it_fly));
                books.add(new Book(6, "Design a Better business", "Eric Vanderbuilt", R.drawable.design_better_business));
                books.add(new Book(7, "Think like Einstein", "Peter Hollins", R.drawable.persuasion_iq));
                books.add(new Book(8, "The AI Advantage", "Thomas H. Davenport", R.drawable.ai_advantage));
                books.add(new Book(9, "Power VS Force", "David R. Hawkins PhD.", R.drawable.power_vs_force));
                books.add(new Book(10, "Persuasion IQ", "Kurt W. Mortensen", R.drawable.persuasion_iq));
                books.add(new Book(11, "The AI Advantage", "Thomas H. Davenport", R.drawable.ai_advantage));
                books.add(new Book(12, "Power VS Force", "David R. Hawkins PhD.", R.drawable.power_vs_force));
                break;

            case BookType.AUDIOBOOK:
                books.add(new Book(1, "Talk like TED", "Carmine Gallo", R.drawable.talk_like_ted));
                books.add(new Book(2, "Never Split the Difference", "Chris Voss", R.drawable.never_split_the_diff));
                books.add(new Book(3, "King Peggy", "Peggylin Bartels", R.drawable.king_peggy_big));
                books.add(new Book(4, "Think like Einstein", "Peter Hollins", R.drawable.think_like_einstein));
                books.add(new Book(5, "Will it Fly?", "Pat Flynn", R.drawable.will_it_fly));
                books.add(new Book(6, "Design a Better business", "Eric Vanderbuilt", R.drawable.design_better_business));
                books.add(new Book(7, "Think like Einstein", "Peter Hollins", R.drawable.persuasion_iq));
                books.add(new Book(8, "The AI Advantage", "Thomas H. Davenport", R.drawable.ai_advantage));
                books.add(new Book(9, "Power VS Force", "David R. Hawkins PhD.", R.drawable.power_vs_force));
                books.add(new Book(10, "Persuasion IQ", "Kurt W. Mortensen", R.drawable.persuasion_iq));
                books.add(new Book(11, "The AI Advantage", "Thomas H. Davenport", R.drawable.ai_advantage));
                books.add(new Book(12, "Power VS Force", "David R. Hawkins PhD.", R.drawable.power_vs_force));
                break;

            case BookType.VIDEO:
                books.add(new Book(1, "Talk like TED", "Carmine Gallo", R.drawable.talk_like_ted));
                books.add(new Book(2, "Never Split the Difference", "Chris Voss", R.drawable.never_split_the_diff));
                books.add(new Book(3, "King Peggy", "Peggylin Bartels", R.drawable.king_peggy_big));
                books.add(new Book(4, "Think like Einstein", "Peter Hollins", R.drawable.think_like_einstein));
                books.add(new Book(5, "Will it Fly?", "Pat Flynn", R.drawable.will_it_fly));
                books.add(new Book(6, "Design a Better business", "Eric Vanderbuilt", R.drawable.design_better_business));
                books.add(new Book(7, "Think like Einstein", "Peter Hollins", R.drawable.persuasion_iq));
                books.add(new Book(8, "The AI Advantage", "Thomas H. Davenport", R.drawable.ai_advantage));
                books.add(new Book(9, "Power VS Force", "David R. Hawkins PhD.", R.drawable.power_vs_force));
                books.add(new Book(10, "Persuasion IQ", "Kurt W. Mortensen", R.drawable.persuasion_iq));
                books.add(new Book(11, "The AI Advantage", "Thomas H. Davenport", R.drawable.ai_advantage));
                books.add(new Book(12, "Power VS Force", "David R. Hawkins PhD.", R.drawable.power_vs_force));
                break;

            case BookType.ARTICLE:
                books.add(new Book(1, "Talk like TED", "Carmine Gallo", R.drawable.talk_like_ted));
                books.add(new Book(2, "Never Split the Difference", "Chris Voss", R.drawable.never_split_the_diff));
                books.add(new Book(3, "King Peggy", "Peggylin Bartels", R.drawable.king_peggy_big));
                books.add(new Book(4, "Think like Einstein", "Peter Hollins", R.drawable.think_like_einstein));
                books.add(new Book(5, "Will it Fly?", "Pat Flynn", R.drawable.will_it_fly));
                books.add(new Book(6, "Design a Better business", "Eric Vanderbuilt", R.drawable.design_better_business));
                books.add(new Book(7, "Think like Einstein", "Peter Hollins", R.drawable.persuasion_iq));
                books.add(new Book(8, "The AI Advantage", "Thomas H. Davenport", R.drawable.ai_advantage));
                books.add(new Book(9, "Power VS Force", "David R. Hawkins PhD.", R.drawable.power_vs_force));
                books.add(new Book(10, "Persuasion IQ", "Kurt W. Mortensen", R.drawable.persuasion_iq));
                books.add(new Book(11, "The AI Advantage", "Thomas H. Davenport", R.drawable.ai_advantage));
                books.add(new Book(12, "Power VS Force", "David R. Hawkins PhD.", R.drawable.power_vs_force));
                break;

        }


        searchResultAdapter = new SearchResultAdapter(getContext(),this,books);
        recyclerViewBooks.setAdapter(searchResultAdapter);


    }

    @Override
    public void onBookSelected(Book book) {
        startActivity(new Intent(getActivity(), BookInfoActivity.class));
    }
}