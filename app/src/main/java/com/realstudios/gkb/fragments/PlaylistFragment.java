package com.realstudios.gkb.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.realstudios.gkb.R;
import com.realstudios.gkb.adapter.PlaylistAdapter;
import com.realstudios.gkb.model.PlayerState;
import com.realstudios.gkb.model.PlaylistItem;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PlaylistFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PlaylistFragment extends Fragment implements PlaylistAdapter.setOnPlaylistListener{

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    PlaylistAdapter playlistAdapter;
    List<PlaylistItem> playlistItems;
    RecyclerView recyclerView;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public PlaylistFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PlaylistFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PlaylistFragment newInstance(String param1, String param2) {
        PlaylistFragment fragment = new PlaylistFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_video_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.recycler_view_playlist);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setNestedScrollingEnabled(false);
        playlistItems = new ArrayList<>();

        playlistAdapter = new PlaylistAdapter(getActivity(),this,getPlaylistItems());
        recyclerView.setAdapter(playlistAdapter);
    }

    public List<PlaylistItem> getPlaylistItems() {
        for (int i=0; i<10;i++){
            int id = i+1;
            playlistItems.add(new PlaylistItem(id, PlayerState.QUEUED,"Data Science part "+id, "starting from scratch with the fundamentals "+id,""));
        }
        return playlistItems;
    }

    @Override
    public void onPlaylistItemSelected(PlaylistItem playlistItem) {


        EventBus.getDefault().post(playlistItem);
        for (int i=0; i<playlistItems.size();i++) {
            PlaylistItem playlistItem1= playlistItems.get(i);

            if(playlistItem1.getId() == playlistItem.getId()) {
                if (playlistItem.getState().equals(PlayerState.PLAYING)) {
                    playlistItems.get(i).setState(PlayerState.PAUSED);
                }
                else {
                    playlistItems.get(i).setState(PlayerState.PLAYING);
                }
            }

            else playlistItems.get(i).setState(PlayerState.PAUSED);
        }
        playlistAdapter.notifyDataSetChanged();




    }
}