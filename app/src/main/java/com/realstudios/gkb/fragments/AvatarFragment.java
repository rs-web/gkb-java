package com.realstudios.gkb.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.realstudios.gkb.R;
import com.realstudios.gkb.adapter.AvatarAdapter;
import com.realstudios.gkb.model.Avatar;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AvatarFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AvatarFragment extends Fragment implements AvatarAdapter.setAvatarListener{

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    List<Avatar> avatarList;
    AvatarAdapter avatarAdapter;
    RecyclerView recyclerView;

    public AvatarFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AvatarFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AvatarFragment newInstance(String param1, String param2) {
        AvatarFragment fragment = new AvatarFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_avatar, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        avatarList = new ArrayList<>();

        recyclerView = view.findViewById(R.id.recycler_view_avatar);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),3));
        //recyclerView.setHasFixedSize(true);
        avatarAdapter = new AvatarAdapter(getAvatarList(),getActivity(),this);
        recyclerView.setAdapter(avatarAdapter);
    }


    public List<Avatar> getAvatarList() {
        avatarList.add(new Avatar(1, R.drawable.joseph_stalin));
        avatarList.add(new Avatar(2, R.drawable.muslim_woman));
        avatarList.add(new Avatar(3, R.drawable.traditional_african_man));
        avatarList.add(new Avatar(4, R.drawable.barack_obama));
        avatarList.add(new Avatar(5, R.drawable.girl_in_ballcap));
        avatarList.add(new Avatar(6, R.drawable.cristiano_ronaldo));
        avatarList.add(new Avatar(7, R.drawable.dave_grohl));
        avatarList.add(new Avatar(8, R.drawable.indian_woman));
        avatarList.add(new Avatar(9, R.drawable.mahatma_gandhi));
        return avatarList;
    }

    @Override
    public void onAvatarSelected(Avatar avatar) {

        for (int i=0; i<avatarList.size();i++) {
            Avatar avatar1= avatarList.get(i);
            avatarList.get(i).setSelected(avatar1.getId() == avatar.getId());
        }
        avatarAdapter.notifyDataSetChanged();
    }
}