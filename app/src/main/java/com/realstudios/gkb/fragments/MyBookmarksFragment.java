package com.realstudios.gkb.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;
import com.realstudios.gkb.ContinueActivity;
import com.realstudios.gkb.DownloadsActivity;
import com.realstudios.gkb.PlayListActivity;
import com.realstudios.gkb.R;
import com.realstudios.gkb.adapter.BookmarksFragmentAdapter;
import com.realstudios.gkb.adapter.BooksFragmentAdapter;
import com.realstudios.gkb.adapter.IconMenuAdapter;
import com.realstudios.gkb.model.BookType;
import com.realstudios.gkb.util.IconPowerMenuItem;
import com.skydoves.powermenu.CustomPowerMenu;
import com.skydoves.powermenu.MenuAnimation;
import com.skydoves.powermenu.OnMenuItemClickListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static com.realstudios.gkb.Utils.convertDpToPixelInt;


public class MyBookmarksFragment extends Fragment {

    TabLayout tabLayout;
    ViewPager2 viewPager;
    CustomPowerMenu customPowerMenu;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public MyBookmarksFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        Log.d("MyNotesFragment", "onCreateView");
        return inflater.inflate(R.layout.fragment_my_bookmarks, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        tabLayout = view.findViewById(R.id.tabLayout);
        viewPager = view.findViewById(R.id.viewPager);


        view.findViewById(R.id.view_menu).setOnClickListener(view1 -> customPowerMenu.showAsDropDown(view1));
        view.findViewById(R.id.btn_menu).setOnClickListener(view1 ->
                customPowerMenu.showAsDropDown(view1));


        List<String> bookTypes = new ArrayList<>();
        bookTypes.add(BookType.BOOKMARK);
        bookTypes.add(BookType.FAVORITE);
        bookTypes.add(BookType.HISTORY);
        BookmarksFragmentAdapter booksFragmentAdapter = new BookmarksFragmentAdapter(getChildFragmentManager(),getLifecycle(),bookTypes);

        viewPager.setUserInputEnabled(true);
        viewPager.setAdapter(booksFragmentAdapter);

        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                tabLayout.selectTab(tabLayout.getTabAt(position));
                super.onPageSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewPager.setCurrentItem(tab.getPosition(),true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition(),true);
            }
        });

        if(getActivity()!=null && isAdded()){
            createMenu(getActivity());
        }
    }

    private void createMenu(Context context){


        customPowerMenu = new CustomPowerMenu.Builder<>(context, new IconMenuAdapter())
                .addItem(new IconPowerMenuItem(ContextCompat.getDrawable(context, R.drawable.ic_download_icon), "DOWNLOADS"))
                .addItem(new IconPowerMenuItem(ContextCompat.getDrawable(context, R.drawable.ic_continue_file), "CONTINUE READING"))
                .addItem(new IconPowerMenuItem(ContextCompat.getDrawable(context, R.drawable.ic_settings_two), "SETTINGS"))
                .setOnMenuItemClickListener((onIconMenuItemClickListener))
                .setAnimation(MenuAnimation.SHOWUP_TOP_RIGHT)
                .setMenuRadius(20f)
                .setWidth(convertDpToPixelInt(300,context))
                .setMenuShadow(2f)
                .build();

    }

    private final OnMenuItemClickListener<IconPowerMenuItem> onIconMenuItemClickListener = (position, item) -> {
        switch (position){
            case 0:
                startActivity(new Intent(getActivity(), DownloadsActivity.class));
                break;
            case 1:
                startActivity(new Intent(getActivity(), ContinueActivity.class));
                break;

            case 2:
               startActivity(new Intent(getActivity(), PlayListActivity.class));
                break;

        }
    };
}