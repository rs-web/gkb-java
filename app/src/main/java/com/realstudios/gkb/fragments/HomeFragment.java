package com.realstudios.gkb.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;
import com.realstudios.gkb.R;
import com.realstudios.gkb.SearchResultActivity;
import com.realstudios.gkb.adapter.HomeFragmentAdapter;
import com.realstudios.gkb.model.BookType;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    TabLayout tabLayout;
    ViewPager2 viewPager;
    AppCompatEditText edtSearch;
    public HomeFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d("Home", "onCreateView");
        return inflater.inflate(R.layout.fragment_home, container, false);
    }


    @Override
    public void onPause() {
        super.onPause();
        Log.d("onPause", "HomeFragment");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("onResume", "HomeFragment");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tabLayout = view.findViewById(R.id.tabLayout);
        viewPager = view.findViewById(R.id.viewPager);
        edtSearch = view.findViewById(R.id.edt_search);


        view.findViewById(R.id.btn_search).setOnClickListener(view1 -> {
            if(edtSearch.getText()!=null){
                if(edtSearch.getText().toString().length()>0){
                    Intent intent = new Intent(getActivity(), SearchResultActivity.class);
                    intent.putExtra("searchString", edtSearch.getText().toString());
                    startActivity(intent);
                }
            }
        });
        List<String> bookTypes = new ArrayList<>();
        bookTypes.add(BookType.RECENTLY_ADDED);
        bookTypes.add(BookType.FEATURED);
        bookTypes.add(BookType.POPULAR_FILES);
        if(getActivity()!=null && isAdded()) {
            final HomeFragmentAdapter adapter = new HomeFragmentAdapter(getChildFragmentManager(),getLifecycle(),bookTypes);
            viewPager.setUserInputEnabled(false);
            viewPager.setAdapter(adapter);

            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {

                    viewPager.setCurrentItem(tab.getPosition(),true);
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {
                    viewPager.setCurrentItem(tab.getPosition(),true);
                }
            });
        }

    }
}