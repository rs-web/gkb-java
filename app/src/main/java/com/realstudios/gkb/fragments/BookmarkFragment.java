package com.realstudios.gkb.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.realstudios.gkb.BookInfoActivity;
import com.realstudios.gkb.R;
import com.realstudios.gkb.adapter.BookmarkAdapter;
import com.realstudios.gkb.adapter.BooksAdapter;
import com.realstudios.gkb.model.Book;
import com.realstudios.gkb.model.BookPage;
import com.realstudios.gkb.model.BookType;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BookmarkFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BookmarkFragment extends Fragment implements BookmarkAdapter.setOnBookListener {
    private static final String BOOK_TYPE = "BOOK_TYPE";
    private String mBOOK_TYPE ="";

    BookmarkAdapter booksAdapter;
    RecyclerView recyclerViewBooks;
    List<Book> books;
    List<BookPage> bookPages;

    public BookmarkFragment() {
        // Required empty public constructor
    }

    public static BookmarkFragment newInstance(String booKType) {
        BookmarkFragment fragment = new BookmarkFragment();
        Bundle args = new Bundle();
        args.putString(BOOK_TYPE,booKType );
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mBOOK_TYPE = getArguments().getString(BOOK_TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bookmark, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerViewBooks = view.findViewById(R.id.recycler_view_books);
        recyclerViewBooks.setNestedScrollingEnabled(false);
        recyclerViewBooks.setLayoutManager(new LinearLayoutManager(getContext()));
       // recyclerViewBooks.setHasFixedSize(true);



        bookPages = new ArrayList<>();
        bookPages.add(new BookPage(1, "Chapter One",1, 1,false));
        bookPages.add(new BookPage(2, "Chapter Two",2, 1,true));
        bookPages.add(new BookPage(3, "Chapter Three",3, 1));
        bookPages.add(new BookPage(4, "Chapter Four",4, 1));
        bookPages.add(new BookPage(5, "Chapter Five",5, 1));
        bookPages.add(new BookPage(6, "Chapter Six",6, 1));
        bookPages.add(new BookPage(7, "Chapter Seven",7, 1));
        bookPages.add(new BookPage(8, "Chapter Eight",8, 1));
        bookPages.add(new BookPage(10, "Chapter Seven",9, 1));
        books = new ArrayList<>();

        switch (mBOOK_TYPE){
            case BookType.BOOKMARK:
                books.add(new Book(1, "Talk like TED", "Carmine Gallo", R.drawable.talk_like_ted,bookPages));
                books.add(new Book(2, "The AI Advantage", "Thomas H. Davenport", R.drawable.ai_advantage,bookPages));
                books.add(new Book(2, "Never Split the Difference", "Chris Voss", R.drawable.never_split_the_diff,bookPages));
                books.add(new Book(4, "Persuasion IQ", "Kurt W. Mortensen", R.drawable.persuasion_iq,bookPages));
                books.add(new Book(5, "The AI Advantage", "Thomas H. Davenport", R.drawable.ai_advantage,bookPages));
                books.add(new Book(6, "Power VS Force", "David R. Hawkins PhD.", R.drawable.power_vs_force,bookPages));
                books.add(new Book(7, "Persuasion IQ", "Kurt W. Mortensen", R.drawable.persuasion_iq,bookPages));
                books.add(new Book(8, "The AI Advantage", "Thomas H. Davenport", R.drawable.ai_advantage,bookPages));
                books.add(new Book(9, "Power VS Force", "David R. Hawkins PhD.", R.drawable.power_vs_force,bookPages));
                books.add(new Book(10, "Persuasion IQ", "Kurt W. Mortensen", R.drawable.persuasion_iq,bookPages));
                books.add(new Book(11, "The AI Advantage", "Thomas H. Davenport", R.drawable.ai_advantage,bookPages));
                books.add(new Book(12, "Power VS Force", "David R. Hawkins PhD.", R.drawable.power_vs_force,bookPages));

                break;

            case BookType.FAVORITE:
                books.add(new Book(1, "Talk like TED", "Carmine Gallo", R.drawable.talk_like_ted,bookPages));
                books.add(new Book(2, "Never Split the Difference", "Chris Voss", R.drawable.never_split_the_diff,bookPages));
                books.add(new Book(3, "King Peggy", "Peggylin Bartels", R.drawable.king_peggy_big,bookPages));
                books.add(new Book(4, "Think like Einstein", "Peter Hollins", R.drawable.think_like_einstein,bookPages));
                books.add(new Book(5, "Will it Fly?", "Pat Flynn", R.drawable.will_it_fly,bookPages));
                books.add(new Book(6, "Design a Better business", "Eric Vanderbuilt", R.drawable.design_better_business,bookPages));
                books.add(new Book(7, "Think like Einstein", "Peter Hollins", R.drawable.persuasion_iq,bookPages));
                books.add(new Book(8, "The AI Advantage", "Thomas H. Davenport", R.drawable.ai_advantage,bookPages));
                books.add(new Book(9, "Power VS Force", "David R. Hawkins PhD.", R.drawable.power_vs_force,bookPages));
                books.add(new Book(10, "Persuasion IQ", "Kurt W. Mortensen", R.drawable.persuasion_iq,bookPages));
                books.add(new Book(11, "The AI Advantage", "Thomas H. Davenport", R.drawable.ai_advantage,bookPages));
                books.add(new Book(12, "Power VS Force", "David R. Hawkins PhD.", R.drawable.power_vs_force,bookPages));
                break;

            case BookType.HISTORY:
                books.add(new Book(1, "Persuasion IQ", "Kurt W. Mortensen", R.drawable.persuasion_iq,bookPages));
                books.add(new Book(2, "The AI Advantage", "Thomas H. Davenport", R.drawable.ai_advantage,bookPages));
                books.add(new Book(2, "Never Split the Difference", "Chris Voss", R.drawable.never_split_the_diff,bookPages));
                books.add(new Book(3, "King Peggy", "Peggylin Bartels", R.drawable.king_peggy_big,bookPages));
                books.add(new Book(5, "The AI Advantage", "Thomas H. Davenport", R.drawable.ai_advantage,bookPages));
                books.add(new Book(6, "Power VS Force", "David R. Hawkins PhD.", R.drawable.power_vs_force,bookPages));
                books.add(new Book(7, "Persuasion IQ", "Kurt W. Mortensen", R.drawable.persuasion_iq,bookPages));
                books.add(new Book(8, "The AI Advantage", "Thomas H. Davenport", R.drawable.ai_advantage,bookPages));
                books.add(new Book(9, "Power VS Force", "David R. Hawkins PhD.", R.drawable.power_vs_force,bookPages));
                books.add(new Book(10, "Persuasion IQ", "Kurt W. Mortensen", R.drawable.persuasion_iq,bookPages));
                books.add(new Book(11, "The AI Advantage", "Thomas H. Davenport", R.drawable.ai_advantage,bookPages));
                books.add(new Book(12, "Power VS Force", "David R. Hawkins PhD.", R.drawable.power_vs_force,bookPages));
                break;


        }


        booksAdapter = new BookmarkAdapter(getContext(),this,books);
        recyclerViewBooks.setAdapter(booksAdapter);
    }

    @Override
    public void onBookSelected(Book book) {
        startActivity(new Intent(getActivity(), BookInfoActivity.class));
    }
}