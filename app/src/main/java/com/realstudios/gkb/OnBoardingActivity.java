package com.realstudios.gkb;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

public class OnBoardingActivity extends AppCompatActivity {

    BottomSheetBehavior<View> sheetBehavior;
    View onBoardingBottomSheet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_onboarding);

        onBoardingBottomSheet = findViewById(R.id.bottom_sheet_onboarding);


        sheetBehavior = BottomSheetBehavior.from(onBoardingBottomSheet);

        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        findViewById(R.id.tv_get_started).setOnClickListener(v -> {

            startActivity(new Intent(this,StartingActivity.class));
            finish();
        });

    }
}
