package com.realstudios.gkb;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.realstudios.gkb.adapter.InstitutionAdapter;
import com.realstudios.gkb.model.Institution;

import java.util.ArrayList;
import java.util.List;

public class FirstTimerInstitutionSelectActivity extends AppCompatActivity implements InstitutionAdapter.setOnInstitutionListener{

    RecyclerView recyclerViewInstitution;
    InstitutionAdapter institutionAdapter;
    List<Institution> institutions;
    AppCompatTextView tvSkip;
    String styledText = "<u>Skip</u> I belong to none";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_timer_institution_select);

        tvSkip = findViewById(R.id.tv_skip);

        tvSkip.setText(Utils.fromHtml(styledText));

        institutions = new ArrayList<>();

        institutions.add(new Institution(1, "Basic", R.color.colorBasic));
        institutions.add(new Institution(2, "Senior\nhigh", R.color.colorSeniorHigh));
        institutions.add(new Institution(3, "Tertiary", R.color.colorTertiary));
        institutions.add(new Institution(4, "Professional", R.color.colorProfessional));

        institutionAdapter = new InstitutionAdapter(institutions,getApplicationContext(),this);
        recyclerViewInstitution = findViewById(R.id.recycler_view_institution);
        recyclerViewInstitution.setLayoutManager(new GridLayoutManager(getApplicationContext(),2));
        recyclerViewInstitution.setAdapter(institutionAdapter);
        institutionAdapter.notifyDataSetChanged();


    }

    @Override
    public void onInstitutionSelected(Institution institution) {
        startActivity(new Intent(FirstTimerInstitutionSelectActivity.this,ProfilePhotoActivity.class));
        finish();
    }
}
