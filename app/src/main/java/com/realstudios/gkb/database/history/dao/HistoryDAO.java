package com.realstudios.gkb.database.history.dao;


import com.realstudios.gkb.database.BasicDAO;

public interface HistoryDAO<T> extends BasicDAO<T> {
    T getLatestEntry();
}
