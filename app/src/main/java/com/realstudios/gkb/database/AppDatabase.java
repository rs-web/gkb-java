package com.realstudios.gkb.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.realstudios.gkb.database.stream.dao.StreamDAO;
import com.realstudios.gkb.database.stream.model.StreamEntity;
import com.realstudios.gkb.database.history.dao.StreamHistoryDAO;
import com.realstudios.gkb.database.history.model.StreamHistoryEntity;
import com.realstudios.gkb.database.stream.dao.StreamStateDAO;
import com.realstudios.gkb.database.stream.model.StreamStateEntity;

import static com.realstudios.gkb.database.Migrations.DB_VER_4;

@TypeConverters({Converters.class})
@Database(
        entities = {StreamEntity.class, StreamHistoryEntity.class, StreamStateEntity.class},
        version = DB_VER_4
)
public abstract class AppDatabase extends RoomDatabase {
    public static final String DATABASE_NAME = "newpipe.db";


    public abstract StreamDAO streamDAO();

    public abstract StreamHistoryDAO streamHistoryDAO();

    public abstract StreamStateDAO streamStateDAO();


}
