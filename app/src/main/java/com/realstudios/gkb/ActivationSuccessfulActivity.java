package com.realstudios.gkb;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;

public class ActivationSuccessfulActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activation_successful);

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                startActivity(new Intent(ActivationSuccessfulActivity.this, FirstTimerInstitutionSelectActivity.class));
                // overridePendingTransition(0, 0);
                finish();
            }
        }, 2000);

    }
}
