package com.realstudios.gkb.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.realstudios.gkb.fragments.BookmarkFragment;
import com.realstudios.gkb.fragments.SearchResultFragment;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class SearchResultFragmentAdapter extends FragmentStateAdapter {

    private List<String> bookTypes;
    private String searchString;
    public SearchResultFragmentAdapter(@NonNull @NotNull FragmentManager fragmentManager,
                                       @NonNull @NotNull Lifecycle lifecycle, List<String>  bookTypes, String searchString) {
        super(fragmentManager, lifecycle);
        this.bookTypes = bookTypes;
        this.searchString = searchString;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return SearchResultFragment.newInstance(bookTypes.get(position),searchString);
    }

    @Override
    public int getItemCount() {
        return bookTypes == null ? 0 : bookTypes.size();
    }
}
