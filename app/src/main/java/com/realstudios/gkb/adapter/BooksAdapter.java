package com.realstudios.gkb.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.realstudios.gkb.R;
import com.realstudios.gkb.model.Book;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.BookViewHolder> {

    private Context context;
    setOnBookListener listener;
    List<Book> books;

    public BooksAdapter(Context context, setOnBookListener listener, List<Book> books) {
        this.context = context;
        this.listener = listener;
        this.books = books;
    }

    @NonNull
    @NotNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new BookViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_book,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull BookViewHolder holder, int position) {

        Book book = books.get(position);
        holder.tvAuthor.setText(book.getAuthor());
        holder.tvTitle.setText(book.getTitle());
        Glide.with(context).load(book.getBookCover()).into(holder.imageViewCover);
        holder.bookView.setOnClickListener(view -> {
            listener.onBookSelected(book);
        });

    }

    @Override
    public int getItemCount() {
        return books ==null ? 0 : books.size();
    }

    static class BookViewHolder extends RecyclerView.ViewHolder{
        View bookView;
        AppCompatImageView imageViewCover;
        AppCompatTextView tvTitle, tvAuthor;
        public BookViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            bookView = itemView.findViewById(R.id.book_view);
            imageViewCover = itemView.findViewById(R.id.img_book_cover);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvAuthor = itemView.findViewById(R.id.tv_author);
        }
    }

    public interface setOnBookListener{
        void onBookSelected(Book book);
    }
}
