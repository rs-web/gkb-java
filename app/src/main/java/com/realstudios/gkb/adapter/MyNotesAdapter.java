package com.realstudios.gkb.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.realstudios.gkb.R;
import com.realstudios.gkb.model.Note;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class MyNotesAdapter extends RecyclerView.Adapter<MyNotesAdapter.NoteViewHolder> {

    private Context context;
    setOnNoteListener listener;
    List<Note> notes;

    public MyNotesAdapter(Context context, setOnNoteListener listener, List<Note> notes) {
        this.context = context;
        this.listener = listener;
        this.notes = notes;
    }

    @NonNull
    @NotNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new NoteViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_note,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull NoteViewHolder holder, int position) {

        Note note = notes.get(position);
        holder.tvDetails.setText(note.getDetails());
        holder.tvTitle.setText(note.getTitle());
        holder.bookView.setOnClickListener(view -> {
            listener.onNoteSelected(note);
        });

    }

    @Override
    public int getItemCount() {
        return notes ==null ? 0 : notes.size();
    }

    static class NoteViewHolder extends RecyclerView.ViewHolder{
        View bookView;
        AppCompatImageView imageViewCover;
        AppCompatTextView tvTitle, tvDetails;
        public NoteViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            bookView = itemView.findViewById(R.id.note_view);
            imageViewCover = itemView.findViewById(R.id.img_book_cover);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvDetails = itemView.findViewById(R.id.tv_details);
        }
    }

    public interface setOnNoteListener{
        void onNoteSelected(Note note);
    }
}
