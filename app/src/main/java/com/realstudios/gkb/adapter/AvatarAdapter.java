package com.realstudios.gkb.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.realstudios.gkb.R;
import com.realstudios.gkb.model.Avatar;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AvatarAdapter extends RecyclerView.Adapter<AvatarAdapter.AvatarViewHolder> {

    List<Avatar> avatarList;
    Context context;
    setAvatarListener listener;

    public interface setAvatarListener{
        void onAvatarSelected(Avatar avatar);
    }

    public AvatarAdapter(List<Avatar> avatarList, Context context, setAvatarListener listener) {
        this.avatarList = avatarList;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @NotNull
    @Override
    public AvatarViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new AvatarViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_avatar,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AvatarViewHolder holder, int position) {

        Avatar avatar = avatarList.get(position);
        holder.itemView.setOnClickListener(view -> {
            listener.onAvatarSelected(avatar);
        });

        if(avatar.isSelected()){
            holder.imageViewAvatarSelected.setVisibility(View.VISIBLE);
        }
        else {
            holder.imageViewAvatarSelected.setVisibility(View.INVISIBLE);
        }
        Glide.with(context).load(avatar.getIcon()).into(holder.circularImageView);


    }

    @Override
    public int getItemCount() {
        return avatarList == null ? 0: avatarList.size();
    }

    static class AvatarViewHolder extends RecyclerView.ViewHolder{

        CircularImageView circularImageView;
        AppCompatImageView imageViewAvatarSelected;
        public AvatarViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            imageViewAvatarSelected = itemView.findViewById(R.id.avatar_selected);
            circularImageView = itemView.findViewById(R.id.img_avatar);
        }
    }
}
