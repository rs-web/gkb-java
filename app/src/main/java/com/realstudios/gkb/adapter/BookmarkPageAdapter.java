package com.realstudios.gkb.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.realstudios.gkb.R;
import com.realstudios.gkb.model.Book;
import com.realstudios.gkb.model.BookPage;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class BookmarkPageAdapter extends RecyclerView.Adapter<BookmarkPageAdapter.BookmarkPageViewHolder> {

    private Context context;
    setOnBookPageListener listener;
    List<BookPage> bookPages;

    public BookmarkPageAdapter(Context context, setOnBookPageListener listener, List<BookPage> bookPages) {
        this.context = context;
        this.listener = listener;
        this.bookPages = bookPages;
    }

    @NonNull
    @NotNull
    @Override
    public BookmarkPageViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new BookmarkPageViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bookmark_page,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull BookmarkPageViewHolder holder, int position) {

        BookPage bookPage = bookPages.get(position);

        if(bookPage.isCurrent()){
            holder.bookView.setCardBackgroundColor(context.getResources().getColor(R.color.currentBookmarkPageColor));
        }
        else {
            holder.bookView.setCardBackgroundColor(context.getResources().getColor(R.color.defaultBookmarkPageColor));
        }

        holder.tvChapterTitle.setText(bookPage.getChapterTitle());
        holder.tvPageNumber.setText("Page "+bookPage.getPageNumber());

    }

    @Override
    public int getItemCount() {
        return bookPages ==null ? 0 : bookPages.size();
    }

    static class BookmarkPageViewHolder extends RecyclerView.ViewHolder{
        MaterialCardView bookView;
        AppCompatTextView tvChapterTitle, tvPageNumber;
        public BookmarkPageViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            bookView = itemView.findViewById(R.id.view_bookmark_page);
            tvChapterTitle = itemView.findViewById(R.id.tv_chapter_title);
            tvPageNumber = itemView.findViewById(R.id.tv_page_number);
        }
    }

    public interface setOnBookPageListener{
        void onBookPageSelected(BookPage bookPage);
    }
}
