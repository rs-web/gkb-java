package com.realstudios.gkb.adapter;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.realstudios.gkb.fragments.BookmarkFragment;
import com.realstudios.gkb.fragments.HomeItemFragment;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class HomeFragmentAdapter extends FragmentStateAdapter {

    private List<String> bookTypes;

    public HomeFragmentAdapter(@NonNull @NotNull FragmentManager fragmentManager,
                               @NonNull @NotNull Lifecycle lifecycle,
                               List<String>  bookTypes) {
        super(fragmentManager, lifecycle);
        this.bookTypes = bookTypes;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {

      return   HomeItemFragment.newInstance(bookTypes.get(position));
    }

    @Override
    public int getItemCount() {

        return bookTypes == null ? 0 : bookTypes.size();
    }
}
