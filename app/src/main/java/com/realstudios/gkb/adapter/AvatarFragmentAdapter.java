package com.realstudios.gkb.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.realstudios.gkb.fragments.AvatarFragment;
import com.realstudios.gkb.fragments.BookmarkFragment;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AvatarFragmentAdapter extends FragmentStateAdapter {

    public AvatarFragmentAdapter(@NonNull @NotNull FragmentManager fragmentManager,
                                 @NonNull @NotNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return AvatarFragment.newInstance("","");
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}
