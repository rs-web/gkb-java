package com.realstudios.gkb.adapter;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.realstudios.gkb.fragments.HomeFragment;
import com.realstudios.gkb.fragments.MyBookmarksFragment;
import com.realstudios.gkb.fragments.MyLibraryFragment;
import com.realstudios.gkb.fragments.MyNotesFragment;

import org.jetbrains.annotations.NotNull;

public class MainFragmentsAdapter extends FragmentStateAdapter {


    public MainFragmentsAdapter(@NonNull @NotNull FragmentManager fragmentManager, @NonNull @NotNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0:
                Log.d("createFragment", "Home");
                return new HomeFragment();
            case 1:
                return new MyLibraryFragment();
            case 2:
                return new MyBookmarksFragment();
            case 3:
                return new MyNotesFragment();
            default:
                return null;
        }
    }

    @Override
    public int getItemCount() {
        return 4;
    }
}
