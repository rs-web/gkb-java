package com.realstudios.gkb.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.realstudios.gkb.R;
import com.realstudios.gkb.model.Book;
import com.realstudios.gkb.model.Download;
import com.realstudios.gkb.model.DownloadState;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class DownloadAdapter extends RecyclerView.Adapter<DownloadAdapter.BookViewHolder> {

    private Context context;
    setOnDownloadListener listener;
    List<Download> downloads;

    public DownloadAdapter(Context context, setOnDownloadListener listener, List<Download> downloads) {
        this.context = context;
        this.listener = listener;
        this.downloads = downloads;
    }

    @NonNull
    @NotNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new BookViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_download_new,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull BookViewHolder holder, int position) {

        Download download = downloads.get(position);
        holder.tvAuthor.setText(download.getBook().getAuthor());
        holder.tvTitle.setText(download.getBook().getTitle());
        Glide.with(context).load(download.getBook().getBookCover()).into(holder.imageViewCover);

        switch (download.getStatus()){
            case DownloadState.COMPLETED:
                holder.imgStatus.setImageResource(R.drawable.success);
                holder.cardViewActionOne.setVisibility(View.GONE);
                holder.tvCancel.setText("Delete");
                holder.viewFlipperDownloadStatus.setDisplayedChild(1);
                holder.imgDelete.setImageResource(R.drawable.ic_delete);
                holder.cardViewActionTwo.setCardBackgroundColor(context.getResources().getColor(R.color.deleteColor));
                break;

            case DownloadState.DOWNLOADING:
                holder.tvCancel.setText("Pause");
                holder.tvRestart.setText("Cancel");
                holder.tvPercentage.setText("26%");
                holder.viewFlipperDownloadStatus.setDisplayedChild(0);
                holder.imgRestart.setImageResource(R.drawable.ic_pause);
                holder.imgDelete.setImageResource(R.drawable.ic_delete);
                holder.cardViewActionOne.setCardBackgroundColor(context.getResources().getColor(R.color.pauseColor));
                holder.cardViewActionTwo.setCardBackgroundColor(context.getResources().getColor(R.color.deleteColor));
                break;

            case DownloadState.FAILED:
                holder.imgStatus.setImageResource(R.drawable.failed);
                holder.tvCancel.setText("Delete");
                holder.tvRestart.setText("Restart");
                holder.viewFlipperDownloadStatus.setDisplayedChild(1);
                holder.imgRestart.setImageResource(R.drawable.ic_restart);
                holder.imgDelete.setImageResource(R.drawable.ic_delete);
                holder.cardViewActionOne.setCardBackgroundColor(context.getResources().getColor(R.color.restartColor));
                holder.cardViewActionTwo.setCardBackgroundColor(context.getResources().getColor(R.color.deleteColor));
                break;

            case DownloadState.QUEUED:
                holder.tvCancel.setText("Pause");
                holder.tvRestart.setText("Delete");
                holder.tvPercentage.setText("0.0%");
                holder.viewFlipperDownloadStatus.setDisplayedChild(0);
                holder.imgRestart.setImageResource(R.drawable.ic_pause);
                holder.imgDelete.setImageResource(R.drawable.ic_delete);
                holder.cardViewActionOne.setCardBackgroundColor(context.getResources().getColor(R.color.pauseColor));
                holder.cardViewActionTwo.setCardBackgroundColor(context.getResources().getColor(R.color.deleteColor));
                break;
        }
        holder.bookView.setOnClickListener(view -> listener.onDownloadSelected(download));

    }

    @Override
    public int getItemCount() {
        return downloads ==null ? 0 : downloads.size();
    }

    static class BookViewHolder extends RecyclerView.ViewHolder{
        View bookView;
        AppCompatImageView imageViewCover;
        AppCompatTextView tvTitle, tvAuthor, tvRestart, tvCancel, tvPercentage;
        MaterialCardView cardViewActionOne,cardViewActionTwo;
        AppCompatImageButton imgDelete, imgRestart;
        AppCompatImageView imgStatus;
        ViewFlipper viewFlipperDownloadStatus;
        public BookViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            bookView = itemView.findViewById(R.id.download_view);
            imageViewCover = itemView.findViewById(R.id.img_book_cover);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvAuthor = itemView.findViewById(R.id.tv_author);
            cardViewActionOne = itemView.findViewById(R.id.view_action_one);
            cardViewActionTwo = itemView.findViewById(R.id.view_action_two);
            imgDelete = itemView.findViewById(R.id.img_delete);
            imgRestart = itemView.findViewById(R.id.img_restart);
            tvRestart =itemView.findViewById(R.id.tv_restart);
            tvCancel =itemView.findViewById(R.id.tv_cancel);
            imgStatus = itemView.findViewById(R.id.img_status);
            tvPercentage= itemView.findViewById(R.id.tv_download_percentage);
            viewFlipperDownloadStatus= itemView.findViewById(R.id.view_flipper_status);

        }
    }

    public interface setOnDownloadListener{
        void onDownloadSelected(Download download);
    }
}
