package com.realstudios.gkb.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.realstudios.gkb.R;
import com.realstudios.gkb.model.Institution;

import java.util.List;

public class InstitutionAdapter extends RecyclerView.Adapter<InstitutionAdapter.InstitutionViewHolder> {

    private List<Institution> institutions;
    private Context context;
    private setOnInstitutionListener listener;

    public InstitutionAdapter(List<Institution> institutions, Context context,setOnInstitutionListener listener) {
        this.institutions = institutions;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public InstitutionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new InstitutionViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_institution,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull InstitutionAdapter.InstitutionViewHolder holder, int position) {

        Institution institution = institutions.get(position);
        holder.tvInstitutionName.setText(institution.getName());

        int colorId = institutions.get(position).getColor();
        int color = context.getResources().getColor(colorId);
        holder.cardViewInstitution.setCardBackgroundColor(color);
        holder.cardViewInstitution.setOnClickListener(v -> listener.onInstitutionSelected(institution));

    }

    @Override
    public int getItemCount() {
        return institutions==null ?0: institutions.size();
    }


    static class InstitutionViewHolder extends RecyclerView.ViewHolder{

        AppCompatTextView tvInstitutionName;
        CardView cardViewInstitution;
        public InstitutionViewHolder(View itemView) {
            super(itemView);

            cardViewInstitution = itemView.findViewById(R.id.card_view_institution);
            tvInstitutionName = itemView.findViewById(R.id.tv_institution_name);
        }

    }

    public interface setOnInstitutionListener {

        void onInstitutionSelected(Institution institution);
    }
}
