package com.realstudios.gkb.adapter;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.realstudios.gkb.R;
import com.realstudios.gkb.model.Book;
import com.realstudios.gkb.model.BookPage;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class BookmarkAdapter extends RecyclerView.Adapter<BookmarkAdapter.BookViewHolder> {

    private Context context;
    setOnBookListener listener;
    List<Book> books;
    float radius;

    public BookmarkAdapter(Context context, setOnBookListener listener, List<Book> books) {
        this.context = context;
        this.listener = listener;
        this.books = books;
        radius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0f, context.getResources().getDisplayMetrics());

    }

    @NonNull
    @NotNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new BookViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bookmark,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull BookViewHolder holder, int position) {

        holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));

        RecyclerView.OnItemTouchListener mScrollTouchListener = new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(@NotNull RecyclerView rv, MotionEvent e) {
                int action = e.getAction();
                switch (action) {
                    case MotionEvent.ACTION_MOVE:
                        rv.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                }
                return false;
            }

            @Override
            public void onTouchEvent(@NotNull RecyclerView rv, @NotNull MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        };
        holder.recyclerView.addOnItemTouchListener(mScrollTouchListener);
        //holder.recyclerView.setNestedScrollingEnabled(true);
        Book book = books.get(position);
        BookmarkPageAdapter bookmarkPageAdapter = new BookmarkPageAdapter(context, bookPage -> {

        }, book.getBookPages());
        holder.recyclerView.setAdapter(bookmarkPageAdapter);

        holder.tvAuthor.setText(book.getAuthor());
        holder.tvTitle.setText(book.getTitle());

        Glide.with(context).load(book.getBookCover()).into(holder.imageViewCover);
        holder.bookView.setOnClickListener(view -> {
            if(holder.recyclerView.getVisibility()==View.VISIBLE) {
                holder.bookView.setStrokeColor(context.getResources().getColor(android.R.color.transparent));
                holder.viewBookmark.setStrokeColor(context.getResources().getColor(android.R.color.transparent));
                holder.viewBookmark.setCardElevation(0);
                radius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0f, context.getResources().getDisplayMetrics());
                holder.viewBookmark.setRadius(radius);
                holder.bookView.setStrokeWidth(0);
                holder.viewBookmark.setStrokeWidth(0);
                holder.recyclerView.setVisibility(View.GONE);
            }
            else {
                radius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4f, context.getResources().getDisplayMetrics());
                holder.viewBookmark.setRadius(radius);
                holder.viewBookmark.setStrokeWidth(1);
                holder.viewBookmark.setCardElevation(4);
                holder.bookView.setStrokeColor(context.getResources().getColor(R.color.strokeColor));
                holder.viewBookmark.setStrokeColor(context.getResources().getColor(R.color.strokeColor));
                holder.bookView.setStrokeWidth(1);
                holder.recyclerView.setVisibility(View.VISIBLE);
            }
            //listener.onBookSelected(book);
        });

    }

    @Override
    public int getItemCount() {
        return books ==null ? 0 : books.size();
    }

    static class BookViewHolder extends RecyclerView.ViewHolder{
        MaterialCardView bookView,viewBookmark;
        AppCompatImageView imageViewCover;
        AppCompatTextView tvTitle, tvAuthor,tvDateTime;
        RecyclerView recyclerView;
        public BookViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            bookView = itemView.findViewById(R.id.bookmark_view);
            imageViewCover = itemView.findViewById(R.id.img_book_cover);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvAuthor = itemView.findViewById(R.id.tv_author);
            tvDateTime = itemView.findViewById(R.id.tv_date_time);
            recyclerView = itemView.findViewById(R.id.recycler_view_books_pages);
            viewBookmark = itemView.findViewById(R.id.view_bookmark);
        }
    }

    public interface setOnBookListener{
        void onBookSelected(Book book);
    }
}
