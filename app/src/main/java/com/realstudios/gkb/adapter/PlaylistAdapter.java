package com.realstudios.gkb.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.realstudios.gkb.R;
import com.realstudios.gkb.model.BookType;
import com.realstudios.gkb.model.PlayerState;
import com.realstudios.gkb.model.PlaylistItem;
import com.realstudios.gkb.model.Reading;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class PlaylistAdapter extends RecyclerView.Adapter<PlaylistAdapter.PlaylistViewHolder> {

    private Context context;
    setOnPlaylistListener listener;
    List<PlaylistItem> playlistItems;

    public PlaylistAdapter(Context context, setOnPlaylistListener listener, List<PlaylistItem> playlistItems) {
        this.context = context;
        this.listener = listener;
        this.playlistItems = playlistItems;
    }

    @NonNull
    @NotNull
    @Override
    public PlaylistViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new PlaylistViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_playlist_queue,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull PlaylistViewHolder holder, int position) {

        PlaylistItem playlistItem = playlistItems.get(position);
        holder.tvTitle.setText(playlistItem.getTitle());
        holder.tvDescription.setText(playlistItem.getDescription());
        holder.playQueueView.setOnClickListener(view -> listener.onPlaylistItemSelected(playlistItem));

        holder.btnPlay.setOnClickListener(view -> listener.onPlaylistItemSelected(playlistItem));
        switch (playlistItem.getState()){
            case PlayerState.ERROR:
            case PlayerState.PAUSED:
            case PlayerState.QUEUED:
                holder.btnPlay.setImageResource(R.drawable.ic_play);
                break;

            case PlayerState.PLAYING:
                holder.btnPlay.setImageResource(R.drawable.ic_pause_new);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return playlistItems ==null ? 0 : playlistItems.size();
    }

    static class PlaylistViewHolder extends RecyclerView.ViewHolder{
        MaterialCardView playQueueView;
        AppCompatImageButton btnPlay;
        AppCompatTextView tvTitle, tvDescription;

        public PlaylistViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            playQueueView = itemView.findViewById(R.id.play_queue_view);
            btnPlay = itemView.findViewById(R.id.btn_play);
            tvDescription = itemView.findViewById(R.id.tv_description);
            tvTitle = itemView.findViewById(R.id.tv_title);
        }
    }

    public interface setOnPlaylistListener{
        void onPlaylistItemSelected(PlaylistItem playlistItem);
    }
}
