package com.realstudios.gkb.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.realstudios.gkb.R;
import com.realstudios.gkb.model.Book;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.BookViewHolder> {

    Context context;
    setOnBookListener listener;
    List<Book> books;

    public SearchResultAdapter(Context context, setOnBookListener listener, List<Book> books) {
        this.context = context;
        this.listener = listener;
        this.books = books;

    }

    @NonNull
    @NotNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new BookViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_result,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull BookViewHolder holder, int position) {

        Book book = books.get(position);
        holder.tvAuthor.setText(book.getAuthor());
        holder.tvTitle.setText(book.getTitle());
        Glide.with(context).load(book.getBookCover()).into(holder.imageViewCover);


    }

    @Override
    public int getItemCount() {
        return books ==null ? 0 : books.size();
    }

    static class BookViewHolder extends RecyclerView.ViewHolder{
        MaterialCardView searchView;
        AppCompatImageView imageViewCover;
        AppCompatTextView tvTitle, tvAuthor,tvDateTime;
        public BookViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            searchView = itemView.findViewById(R.id.search_view);
            imageViewCover = itemView.findViewById(R.id.img_book_cover);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvAuthor = itemView.findViewById(R.id.tv_author);
            tvDateTime = itemView.findViewById(R.id.tv_date_time);
        }
    }

    public interface setOnBookListener{
        void onBookSelected(Book book);
    }
}
