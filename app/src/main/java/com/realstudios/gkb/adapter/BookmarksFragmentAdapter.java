package com.realstudios.gkb.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.realstudios.gkb.fragments.BookFragment;
import com.realstudios.gkb.fragments.BookmarkFragment;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class BookmarksFragmentAdapter extends FragmentStateAdapter {

    private List<String> bookTypes;
    public BookmarksFragmentAdapter(@NonNull @NotNull FragmentManager fragmentManager,
                                    @NonNull @NotNull Lifecycle lifecycle, List<String>  bookTypes) {
        super(fragmentManager, lifecycle);
        this.bookTypes = bookTypes;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return BookmarkFragment.newInstance(bookTypes.get(position));
    }

    @Override
    public int getItemCount() {
        return bookTypes == null ? 0 : bookTypes.size();
    }
}
