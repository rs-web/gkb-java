package com.realstudios.gkb.adapter;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.realstudios.gkb.fragments.NewNoteFragment;
import com.realstudios.gkb.fragments.NoteFragment;

import org.jetbrains.annotations.NotNull;

public class MyNotesFragmentAdapter extends FragmentStateAdapter {


    public MyNotesFragmentAdapter(@NonNull @NotNull FragmentManager fragmentManager,
                                  @NonNull @NotNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }


    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0:
                Log.d("createFragment", "Home");
                return new NoteFragment();
            case 1:
                return new NewNoteFragment();
            default:
                return null;
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
