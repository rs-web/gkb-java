package com.realstudios.gkb.adapter;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.realstudios.gkb.fragments.CommentsFragment;
import com.realstudios.gkb.fragments.DetailsFragment;
import com.realstudios.gkb.fragments.HomeFragment;
import com.realstudios.gkb.fragments.InteractionFragment;
import com.realstudios.gkb.fragments.MyBookmarksFragment;
import com.realstudios.gkb.fragments.MyLibraryFragment;
import com.realstudios.gkb.fragments.MyNotesFragment;
import com.realstudios.gkb.fragments.PlaylistFragment;

import org.jetbrains.annotations.NotNull;

public class PlayerFragmentsAdapter extends FragmentStateAdapter {


    public PlayerFragmentsAdapter(@NonNull @NotNull FragmentManager fragmentManager, @NonNull @NotNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0:
                Log.d("createFragment", "DetailsFragment");
                return new DetailsFragment();
            case 1:
                return new PlaylistFragment();
            case 2:
                return new InteractionFragment();
            case 3:
                return new CommentsFragment();
            default:
                return null;
        }
    }

    @Override
    public int getItemCount() {
        return 4;
    }
}
