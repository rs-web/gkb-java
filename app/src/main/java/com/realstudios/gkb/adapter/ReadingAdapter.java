package com.realstudios.gkb.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.realstudios.gkb.R;
import com.realstudios.gkb.model.BookType;
import com.realstudios.gkb.model.Note;
import com.realstudios.gkb.model.Reading;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ReadingAdapter extends RecyclerView.Adapter<ReadingAdapter.NoteViewHolder> {

    private Context context;
    setOnReadingListener listener;
    List<Reading> readingList;

    public ReadingAdapter(Context context, setOnReadingListener listener, List<Reading> readingList) {
        this.context = context;
        this.listener = listener;
        this.readingList = readingList;
    }

    @NonNull
    @NotNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new NoteViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_reading,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull NoteViewHolder holder, int position) {

        Reading reading = readingList.get(position);

        holder.tvTitle.setText(reading.getBook().getTitle());
        holder.tvAuthor.setText(reading.getBook().getAuthor());
        Glide.with(context).load(reading.getBook().getBookCover()).into(holder.imageViewCover);
        holder.readingView.setOnClickListener(view -> listener.onReadingSelected(reading));
        switch (reading.getBook().getBookType()){
            case BookType.ARTICLE:
            case BookType.BOOK:
                holder.tvType.setText("Reading");
                break;

            case BookType.AUDIOBOOK:
                holder.tvType.setText("Playing");
                break;


            case BookType.VIDEO:
                holder.tvType.setText("Watching");
                break;
        }


    }

    @Override
    public int getItemCount() {
        return readingList ==null ? 0 : readingList.size();
    }

    static class NoteViewHolder extends RecyclerView.ViewHolder{
        MaterialCardView readingView;
        AppCompatImageView imageViewCover;
        AppCompatTextView tvTitle, tvAuthor, tvType;
        public NoteViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            readingView = itemView.findViewById(R.id.reading_view);
            imageViewCover = itemView.findViewById(R.id.img_book_cover);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvAuthor = itemView.findViewById(R.id.tv_author);
            tvType = itemView.findViewById(R.id.tv_type);
        }
    }

    public interface setOnReadingListener{
        void onReadingSelected(Reading reading);
    }
}
