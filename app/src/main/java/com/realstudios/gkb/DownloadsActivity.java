package com.realstudios.gkb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.realstudios.gkb.adapter.DownloadAdapter;
import com.realstudios.gkb.model.Book;
import com.realstudios.gkb.model.Download;
import com.realstudios.gkb.model.DownloadState;
import com.realstudios.gkb.util.swipe.SlidingItemMenuRecyclerView;

import java.util.ArrayList;
import java.util.List;

public class DownloadsActivity extends AppCompatActivity implements DownloadAdapter.setOnDownloadListener{

    SlidingItemMenuRecyclerView recyclerView;
    DownloadAdapter adapter;
    List<Download> downloads;
    List<Book> books;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloads);

        recyclerView = findViewById(R.id.recycler_view_downloads);

        findViewById(R.id.view_back).setOnClickListener(view -> finish());
        findViewById(R.id.btn_back).setOnClickListener(view -> finish());

        books = new ArrayList<>();
        downloads = new ArrayList<>();



        books.add(new Book(1, "Talk like TED", "Carmine Gallo", R.drawable.talk_like_ted));
        books.add(new Book(2, "Never Split the Difference", "Chris Voss", R.drawable.never_split_the_diff));
        books.add(new Book(3, "King Peggy", "Peggylin Bartels", R.drawable.king_peggy_big));
        books.add(new Book(4, "Think like Einstein", "Peter Hollins", R.drawable.think_like_einstein));
        books.add(new Book(5, "Will it Fly?", "Pat Flynn", R.drawable.will_it_fly));
        books.add(new Book(6, "Design a Better business", "Eric Vanderbuilt", R.drawable.design_better_business));
        books.add(new Book(7, "Think like Einstein", "Peter Hollins", R.drawable.persuasion_iq));
        books.add(new Book(8, "The AI Advantage", "Thomas H. Davenport", R.drawable.ai_advantage));
        books.add(new Book(9, "Power VS Force", "David R. Hawkins PhD.", R.drawable.power_vs_force));
        books.add(new Book(10, "Persuasion IQ", "Kurt W. Mortensen", R.drawable.persuasion_iq));
        books.add(new Book(11, "The AI Advantage", "Thomas H. Davenport", R.drawable.ai_advantage));
        books.add(new Book(12, "Power VS Force", "David R. Hawkins PhD.", R.drawable.power_vs_force));


        for (int i = 0; i<books.size(); i++){
            if(i==0)
            downloads.add(new Download(i,books.get(i),23.6, DownloadState.FAILED));

            else if(i==1)downloads.add(new Download(i,books.get(i),23.6, DownloadState.COMPLETED));

            else if(i==2)downloads.add(new Download(i,books.get(i),23.6, DownloadState.QUEUED));

            else if(i==3)downloads.add(new Download(i,books.get(i),23.6, DownloadState.DOWNLOADING));
            else{
                if(i%2==0)downloads.add(new Download(i,books.get(i),23.6, DownloadState.FAILED));
                else downloads.add(new Download(i,books.get(i),23.6, DownloadState.COMPLETED));
            }
        }

        adapter = new DownloadAdapter(getApplicationContext(),this, downloads);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDownloadSelected(Download download) {

    }
}